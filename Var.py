#CONST
WINDOW_TITLE = "CANable"
LOGO = """
        <head>
        <style>
        * {
        text-align: right;
        }
        .Logo {
        font: 32px;
        }
        </style>
        </head>
        <body>
        <div class="Logo"><font color="red">C</font>ANable</div>
        <div class="From">from <font color="red">Y</font>andex.Auto with <font color="red">love</font></div>
        </body>
        """
VERSION_NAME = """
        <head>
        <style>
        * {
        text-align: right;
        font: 12px;
        }
        </head>
        </style>
        <body>
        <div>V2.0</div>
        </body>
"""
VERSION_INFO = "Pre-stable build v1.5"
LABEL_AVTO = """
        <head>
        <style>
        * {
        color: green;
        }
        </head>
        </style>
        <body>
        <div>Choose auto from toolbar</div>
        </body>
"""
LABEL_CURRENT_AUTO = "Current auto: "
LABEL_COMBO_BOX = "Command for send"
LABEL_COMBO_BOX_CUSTOM = "Bitrate"
NAME_CLEAR_BUTTON = "Clear console"
NAME_SEND_BUTTON = "Send command"
NAME_START_THREAD_BUTTON = "Start"
NAME_STOP_THREAD_BUTTON = "Stop"
STATUS_COMBO_BOX = "Choose command"
STATUS_CLEAR_BUTTON = "Clear all"
STATUS_SEND_BUTTON = "Send CAN-command"
STATUS_START_THREAD_BUTTON = "Start CAN-thread"
STATUS_STOP_THREAD_BUTTON = "Stop CAN-thread"

# #Themes style sheets
# DARK_THEME = open('style.css').read()

nOfCombo = 0

#Thread's flag
thread_flag = "off"

#Custom command flag
custom_command_flag = "off"
custom_command_send_flag = "off"
bitrate = ''
id = ''
dlc = ''
data = ''

#Polo's flags
polo_flag = "off"
polo_volume_up_flag = "off"
polo_volume_down_flag = "off"
polo_voice_flag = "off"
polo_mute_flag = "off"
polo_phone_flag = "off"
polo_next_track_flag = "off"
polo_pre_track_flag = "off"
polo_reversing_on_flag = "off"
polo_reversing_off_flag = "off"
polo_lighting_on_flag = "off"
polo_lighting_off_flag = "off"


#Octavia's flags
octavia_flag = "off"
octavia_volume_up_flag = "off"
octavia_volume_down_flag = "off"
octavia_voice_flag = "off"
octavia_mute_flag = "off"
octavia_phone_flag = "off"
octavia_next_track_flag = "off"
octavia_next_long_press_flag = "off"
octavia_pre_track_flag = "off"
octavia_pre_long_press_flag = "off"


#Nissan's flags
nissan_flag = "off"
nissan_volume_up_flag = "off"
nissan_volume_down_flag = "off"
nissan_hang_up_flag = "off"
nissan_mode_flag = "off"
nissan_next_track_flag = "off"
nissan_pre_tack_flag = "off"
nissan_acc_on_flag = "off"
nissan_acc_off_flag = "off"
nissan_phone_voice_long_press_flag = "off"
nissan_phone_voice_short_press_flag = "off"


#Mitsubishi's flags
mitsubishi_flag = "off"
mitsubishi_reversing_on_flag = "off"
mitsubishi_reversing_off_flag = "off"
mitsubishi_foot_windscreen_flag = "off"
mitsubishi_face_flag = "off"
mitsubishi_face_foot_flag = "off"
mitsubishi_foot_flag = "off"
mitsubishi_dual_flag = "off"
mitsubishi_recirculation_cabin_flag = "off"
mitsubishi_recirculation_outside_flag = "off"
mitsubishi_acc_on_flag = "off"
mitsubishi_acc_off_flag = "off"
mitsubishi_windscreen_heating_on_flag = "off"
mitsubishi_windscreen_heating_off_flag = "off"
mitsubishi_rear_window_heating_on_flag = "off"
mitsubishi_rear_window_heating_off_flag = "off"
mitsubishi_auto_on_flag = "off"
mitsubishi_auto_off_flag = "off"
mitsubishi_ventilator_power_flag = "off"
mitsubishi_left_tem_regulation_flag = "off"
mitsubishi_right_tem_regulation_flag = "off"


#Vesta's flags
vesta_flag = "off"
vesta_Handbrake_command_flag = "off"
vesta_speed_5_km_h_comand_flag = "off"
vesta_speed_15_km_h_command_flag = "off"
vesta_reversing_on_command_flag = "off"
vesta_reversing_off_command_flag = "off"
vesta_parktronic_zero_position_command_flag = "off"
vesta_parktronic_leftmost_position_command_flag = "off"
vesta_parktronic_rightmost_position_command_flag = "off"
vesta_external_sensor_temperature_minus_18 = "off"
vesta_external_sensor_temperature_0 = "off"
vesta_external_sensor_temperature_plus_12 = "off"
vesta_open_drivers_door = "off"
vesta_open_passengers_door = "off"
vesta_open_back_door = "off"
vesta_open_hood = "off"
vesta_open_trunk = "off"
vesta_5_commands_flag = "off"
vesta_4_commands_flag = "off"
vesta_3_commands_flag = "off"
vesta_2_commands_flag = "off"
command = ''
command_1 = ''
command_2 = ''
command_3 = ''

#Granta's flags
granta_flag = "off"
granta_handbrake_flag = "off"
granta_speed_5_km_h_flag = "off"
granta_speed_15_km_h_flag = "off"
granta_external_sensor_temperature_minus_18 = "off"
granta_external_sensor_temperature_0 = "off"
granta_external_sensor_temperature_plus_12 = "off"


#Arkana's flags
arkana_flag = "off"
arkana_voice_command_flag = "off"
arkana_Handbrake_command_flag = "off"
