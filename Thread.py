import threading
import time
from can import Bus

import Var

import Gui

from car_methods.arkana_commands import Arkana
from car_methods.polo_commands import Polo
from car_methods.nissan_commands import Nissan
from car_methods.mitsubishi_commands import Mitsubishi
from car_methods.octavia_rapid_commands import Octavia_Rapid
from car_methods.vesta_commands import Vesta
from car_methods.granta_commands import Granta
from car_methods.custom_command import CustomCommand
from serial_port_connection.connection_mode import auto_port_scan

from car_methods.base_constance import polo_can_bitrate, octavia_can_bitrate, nissan_can_bitrate, \
    mitsubishi_can_bitrate, bus_type, vesta_can_bitrate, granta_can_bitrate, arkana_can_bitrate


class ThreadPolo(threading.Thread):

    def __init__(self, name):
        """Инициализация потока"""
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        """Запуск потока"""
        print("Thread Run")
        bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=polo_can_bitrate)
        while 1 > 0:
            if Var.polo_flag == 'on':
                Polo().wake_up(bus)
            if Var.polo_volume_up_flag == "on":
                Polo().volume_up(bus)
                print("Volume up")
                Var.polo_volume_up_flag = "off"
            if Var.polo_volume_down_flag == "on":
                Polo().volume_down(bus)
                print("Volume down")
                Var.polo_volume_down_flag = "off"
            if Var.polo_voice_flag == "on":
                Polo().voice(bus)
                print("Voice")
                Var.polo_voice_flag = "off"
            if Var.polo_mute_flag == "on":
                Polo().mute(bus)
                print("Mute")
                Var.polo_mute_flag = "off"
            if Var.polo_phone_flag == "on":
                Polo().phone(bus)
                print("Phone")
                Var.polo_phone_flag = "off"
            if Var.polo_next_track_flag == "on":
                Polo().next_track(bus)
                print("Next track")
                Var.polo_next_track_flag = "off"
            if Var.polo_pre_track_flag == "on":
                Polo().pre_track(bus)
                print("Pre track")
                Var.polo_pre_track_flag = "off"
            if Var.polo_reversing_on_flag == "on":
                Polo().reversing_on(bus)
                print("Reversing on")
                Var.polo_reversing_on_flag = "off"
            if Var.polo_reversing_off_flag == "on":
                Polo().reversing_off(bus)
                print("Reversing on")
                Var.polo_reversing_off_flag = "off"
            if Var.polo_lighting_on_flag == "on":
                Polo().ILL_on(bus)
                print("Lighting on")
                Var.polo_lighting_on_flag = "off"
            if Var.polo_lighting_off_flag == "on":
                Polo().ILL_off(bus)
                print("Lighting off")
                Var.polo_lighting_off_flag = "off"
            if Var.thread_flag == "off":
                print("Thread OFF")
                break


class ThreadOctavia(threading.Thread):

    def __init__(self, name):
        """Инициализация потока"""
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        """Запуск потока"""
        print("Thread Run")
        bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=octavia_can_bitrate)
        while 1 > 0:
            if Var.octavia_volume_up_flag == "on":
                Octavia_Rapid().volume_up(bus)
                print("Volume up")
                Var.octavia_volume_up_flag = "off"
            if Var.octavia_volume_down_flag == "on":
                Octavia_Rapid().volume_down(bus)
                print("Volume down")
                Var.octavia_volume_down_flag = "off"
            if Var.octavia_voice_flag == "on":
                Octavia_Rapid().voice(bus)
                print("Voice")
                Var.octavia_voice_flag = "off"
            if Var.octavia_mute_flag == "on":
                Octavia_Rapid().mute(bus)
                print("Mute")
                Var.octavia_mute_flag = "off"
            if Var.octavia_phone_flag == "on":
                Octavia_Rapid().phone(bus)
                print("Phone")
                Var.octavia_phone_flag = "off"
            if Var.octavia_next_track_flag == "on":
                Octavia_Rapid().next_track(bus)
                print("Next track")
                Var.octavia_next_track_flag = "off"
            if Var.octavia_next_long_press_flag == "on":
                Octavia_Rapid().next_long_press(bus)
                print("Next long press")
                Var.octavia_next_long_press_flag = "off"
            if Var.octavia_pre_track_flag == "on":
                Octavia_Rapid().pre_track(bus)
                print("Pre track")
                Var.octavia_pre_track_flag = "off"
            if Var.octavia_pre_long_press_flag == "on":
                Octavia_Rapid().pre_long_press(bus)
                print("Pre long press")
                Var.octavia_pre_long_press_flag = "off"
            if Var.thread_flag == "off":
                print("Thread OFF")
                break


class ThreadNissan(threading.Thread):

    def __init__(self, name):
        """Инициализация потока"""
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        """Запуск потока"""
        print("Thread Run")
        bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=nissan_can_bitrate)
        while 1 > 0:
            if Var.nissan_volume_up_flag == "on":
                Nissan().volume_up(bus)
                print("Volume up")
                Var.nissan_volume_up_flag = "off"
            if Var.nissan_volume_down_flag == "on":
                Nissan().volume_down(bus)
                print("Volume down")
                Var.nissan_volume_down_flag = "off"
            if Var.nissan_phone_voice_long_press_flag == "on":
                Nissan().voice_phone_long_press(bus)
                print("Phone/Voice Long Press")
                Var.nissan_phone_voice_long_press_flag = "off"
            if Var.nissan_phone_voice_short_press_flag == "on":
                Nissan().voice_phone_short_press(bus)
                print("Phone/Voice Short Press")
                Var.nissan_phone_voice_short_press_flag = "off"
            if Var.nissan_hang_up_flag == "on":
                Nissan().hang_up(bus)
                print("Hang up")
                Var.nissan_hang_up_flag = "off"
            if Var.nissan_mode_flag == "on":
                Nissan().mode(bus)
                print("Mode")
                Var.nissan_mode_flag = "off"
            if Var.nissan_next_track_flag == "on":
                Nissan().next_track(bus)
                print("Next track")
                Var.nissan_next_track_flag = "off"
            if Var.nissan_pre_tack_flag == "on":
                Nissan().pre_track(bus)
                print("Pre track")
                Var.nissan_pre_tack_flag = "off"
            if Var.nissan_acc_on_flag == "on":
                Nissan().acc_on(bus)
                print("Acc on")
                Var.nissan_acc_on_flag = "off"
            if Var.nissan_acc_off_flag == "on":
                Nissan().acc_off(bus)
                print("Acc off")
                Var.nissan_acc_off_flag = "off"
            if Var.thread_flag == "off":
                print("Thread OFF")
                break


class ThreadMitsubishi(threading.Thread):

    def __init__(self, name):
        """Инициализация потока"""
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        """Запуск потока"""
        print("Thread Run")
        bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=mitsubishi_can_bitrate)
        while 1 > 0:
            if Var.mitsubishi_reversing_on_flag == "on":
                Mitsubishi().reversing_on(bus)
                print("Reversing on")
                Var.mitsubishi_reversing_on_flag = "off"
            if Var.mitsubishi_reversing_off_flag == "on":
                Mitsubishi().reversing_off(bus)
                print("Reversing off")
                Var.mitsubishi_reversing_off_flag = "off"
            if Var.mitsubishi_foot_windscreen_flag == "on":
                Mitsubishi().foot_windscreen(bus)
                print("Foot windscreen")
                Var.mitsubishi_foot_windscreen_flag = "off"
            if Var.mitsubishi_face_flag == "on":
                Mitsubishi().face(bus)
                print("Face")
                Var.mitsubishi_face_flag = "off"
            if Var.mitsubishi_face_foot_flag == "on":
                Mitsubishi().face_foot(bus)
                print("Face foot")
                Var.mitsubishi_face_foot_flag = "off"
            if Var.mitsubishi_foot_flag == "on":
                Mitsubishi().foot(bus)
                print("Foot")
                Var.mitsubishi_foot_flag = "off"
            if Var.mitsubishi_dual_flag == "on":
                Mitsubishi().dual(bus)
                print("Dual")
                Var.mitsubishi_dual_flag = "off"
            if Var.mitsubishi_recirculation_cabin_flag == "on":
                Mitsubishi().recirculation_cabin(bus)
                print("Recirculation cabin")
                Var.mitsubishi_recirculation_cabin_flag = "off"
            if Var.mitsubishi_recirculation_outside_flag == "on":
                Mitsubishi().recirculation_outside(bus)
                print("Recirculation outside")
                Var.mitsubishi_recirculation_outside_flag = "off"
            if Var.mitsubishi_acc_on_flag == "on":
                Mitsubishi().ac_on(bus)
                print("Acc on")
                Var.mitsubishi_acc_on_flag = "off"
            if Var.mitsubishi_acc_off_flag == "on":
                Mitsubishi().ac_off(bus)
                print("Acc of")
                Var.mitsubishi_acc_off_flag = "off"
            if Var.mitsubishi_windscreen_heating_on_flag == "on":
                Mitsubishi().windscreen_heating_on(bus)
                print("Windscreen heating on")
                Var.mitsubishi_windscreen_heating_on_flag = "off"
            if Var.mitsubishi_windscreen_heating_off_flag == "on":
                Mitsubishi().windscreen_heating_off(bus)
                print("Windscreen heating off")
                Var.mitsubishi_windscreen_heating_off_flag = "off"
            if Var.mitsubishi_rear_window_heating_on_flag == "on":
                Mitsubishi().rear_window_heating_on(bus)
                print("Rear window heating on")
                Var.mitsubishi_rear_window_heating_on_flag = "off"
            if Var.mitsubishi_rear_window_heating_off_flag == "on":
                Mitsubishi().rear_window_heating_off(bus)
                print("Rear window heating off")
                Var.mitsubishi_rear_window_heating_off_flag = "off"
            if Var.mitsubishi_auto_on_flag == "on":
                Mitsubishi().auto_on(bus)
                print("Auto on")
                Var.mitsubishi_auto_on_flag = "off"
            if Var.mitsubishi_auto_off_flag == "on":
                Mitsubishi().auto_off(bus)
                print("Auto off")
                Var.mitsubishi_auto_off_flag = "off"
            if Var.mitsubishi_ventilator_power_flag == "on":
                Mitsubishi().ventilator_power(bus)
                print("Ventilator power")
                Var.mitsubishi_ventilator_power_flag = "off"
            if Var.mitsubishi_left_tem_regulation_flag == "on":
                Mitsubishi().left_tem_regulation(bus)
                print("Left tem regulation")
                Var.mitsubishi_left_tem_regulation_flag = "off"
            if Var.mitsubishi_right_tem_regulation_flag == "on":
                Mitsubishi().right_tem_regulation(bus)
                print("Right tem regulation")
                Var.mitsubishi_right_tem_regulation_flag = "off"
            if Var.thread_flag == "off":
                print("Thread off")
                break


class ThreadArkana(threading.Thread):

    def __init__(self, name):
        """Инициализация потока"""
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        """Запуск потока"""
        print("Thread Run")
        bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=arkana_can_bitrate)
        while 1 > 0:
            if Var.arkana_flag == "on":
                Arkana().wake_up(bus)
                time.sleep(0.01)
                if Var.arkana_voice_command_flag == "on":
                    Arkana().voice(bus)
                    print("Voice")
                    Var.arkana_voice_command_flag = "off"
                if Var.arkana_Handbrake_command_flag == "on":
                    Arkana().handbrake(bus)
                    print("Handbrake")
                    Var.arkana_Handbrake_command_flag = "off"
            if Var.thread_flag == "off":
                print("Thread off")
                break


class ThreadGranta(threading.Thread):

    def __init__(self, name):
        """Инициализация потока"""
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        """Запуск потока"""
        print("Thread Run")
        bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=granta_can_bitrate)
        while 1 > 0:
            if Var.granta_handbrake_flag == "on":
                Granta().handbrake(bus)
                print("Handbrake")
                Var.granta_handbrake_flag = "off"
            if Var.granta_speed_5_km_h_flag == "on":
                Granta().Speed_5_km_h(bus)
                print("Speed 5 km/h")
                Var.granta_speed_5_km_h_flag = "off"
            if Var.granta_speed_15_km_h_flag == "on":
                Granta().Speed_15_km_h(bus)
                print("Speed 15 km/h")
                Var.granta_speed_15_km_h_flag = "off"
            if Var.granta_external_sensor_temperature_minus_18 == "on":
                Granta().external_sensor_temperature_minus_18(bus)
                print("External Sensor Temperature -18")
                Var.granta_external_sensor_temperature_minus_18 = "off"
            if Var.granta_external_sensor_temperature_0 == "on":
                Granta().external_sensor_temperature_0(bus)
                print("External Sensor Temperature 0")
                Var.granta_external_sensor_temperature_0 = "off"
            if Var.granta_external_sensor_temperature_plus_12 == "on":
                Granta().external_sensor_temperature_plus_12(bus)
                print("External Sensor Temperature +12")
                Var.granta_external_sensor_temperature_plus_12 = "off"
            if Var.thread_flag == "off":
                print("Thread off")
                break


class ThreadVesta(threading.Thread):

    def __init__(self, name):
        """Инициализация потока"""
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        """Запуск потока"""
        print('Thread run')
        bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=vesta_can_bitrate)
        while 1 > 0:
            if Var.vesta_flag == 'on':
                # Vesta().wake_up(bus)
                time.sleep(0.1)
                if Var.vesta_Handbrake_command_flag == "on":
                    Vesta().handbrake(bus)
                    print("Handbrake")
                    Var.vesta_Handbrake_command_flag = "off"
                if Var.vesta_speed_5_km_h_comand_flag == "on":
                    Vesta().Speed_5_km_h(bus)
                    print("Speed 5km/h")
                    Var.vesta_speed_5_km_h_comand_flag = "off"
                if Var.vesta_speed_15_km_h_command_flag == "on":
                    Vesta().Speed_15_km_h(bus)
                    print("Speed 15km/h")
                    Var.vesta_speed_15_km_h_command_flag = "off"
                if Var.vesta_reversing_on_command_flag == "on":
                    Vesta().Reversing_on(bus)
                    print("Reversing on")
                    Var.vesta_reversing_on_command_flag = "off"
                if Var.vesta_reversing_off_command_flag == "on":
                    Vesta().Reversing_off(bus)
                    print("Reversing off")
                    Var.vesta_reversing_off_command_flag = "off"
                if Var.vesta_parktronic_zero_position_command_flag == "on":
                    Vesta().Parktronic_zero_position(bus)
                    print("Parktronic zero position")
                    Var.vesta_parktronic_zero_position_command_flag = "off"
                if Var.vesta_parktronic_leftmost_position_command_flag == "on":
                    Vesta().Parktronic_leftmost_position(bus)
                    print("Parktronic leftmost position")
                    Var.vesta_parktronic_leftmost_position_command_flag = "off"
                if Var.vesta_parktronic_rightmost_position_command_flag == "on":
                    Vesta().Parktronic_rightmost_position(bus)
                    print("Parktronic rightmost position")
                    Var.vesta_parktronic_rightmost_position_command_flag = "off"
                if Var.vesta_external_sensor_temperature_minus_18 == "on":
                    Vesta().external_sensor_temperature_minus_18(bus)
                    print("External Sensor Temperature -18")
                    Var.vesta_external_sensor_temperature_minus_18 = "off"
                if Var.vesta_external_sensor_temperature_0 == "on":
                    Vesta().external_sensor_temperature_0(bus)
                    print("External Sensor Temperature 0")
                    Var.vesta_external_sensor_temperature_0 = "off"
                if Var.vesta_external_sensor_temperature_plus_12 == "on":
                    Vesta().external_sensor_temperature_plus_12(bus)
                    print("External Sensor Temperature +12")
                    Var.vesta_external_sensor_temperature_plus_12 = "off"
                if Var.vesta_5_commands_flag == "on":
                    Vesta.Hood_Trunk_Doors(Vesta, bus, Var.vesta_open_trunk, Var.vesta_open_hood,
                                                    Var.vesta_open_back_door, Var.vesta_open_drivers_door,
                                                    Var.vesta_open_passengers_door)
                    Var.vesta_5_commands_flag = "off"
                    Var.vesta_open_passengers_door = "off"
                    Var.vesta_open_drivers_door = "off"
                    Var.vesta_open_back_door = "off"
                    Var.vesta_open_trunk = "off"
                    Var.vesta_open_hood = "off"
            if Var.thread_flag == 'off':
                print('Thread off')
                break

class ThreadCustom(threading.Thread):
    def __init__(self, name):
        """Инициализация потока"""
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        """Запуск потока"""
        print('Thread run')
        bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=Var.bitrate)
        while 1 > 0:
            if Var.custom_command_flag == "on":
                if Var.custom_command_send_flag == "on":
                    print("bus_type:", Var.bitrate)
                    print("Var.id", Var.id)
                    print("Var.dlc", Var.dlc)
                    print("Var.data", Var.data)
                    CustomCommand(Var.id, Var.dlc, Var.data).sendCommand(bus)
                    Var.custom_command_send_flag = "off"
            if Var.thread_flag == 'off':
                print('Thread off')
                break


