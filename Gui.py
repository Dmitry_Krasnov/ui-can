#!/usr/bin/python3
# -*- coding: utf-8 -*-

import Var
import sys
import time

from serial_port_connection.connection_mode import auto_port_scan
#import can_parser.parser_for_can_hacker_files
from Thread import ThreadPolo, ThreadOctavia, ThreadNissan, ThreadMitsubishi, ThreadVesta, ThreadGranta, ThreadArkana, ThreadCustom

from PyQt5.QtWidgets import QApplication, QMainWindow, QAction, qApp, QPushButton, QComboBox,  \
    QFileDialog, QLabel, QTextBrowser, QCheckBox, QTabWidget, QMenuBar, QStatusBar, QFrame, QLineEdit
from PyQt5.QtGui import QColor, QPalette
from PyQt5 import Qt, QtCore
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtGui import QIcon, QIntValidator
from pathlib import Path

from car_methods.vesta_commands import Vesta

class GUI_CAN_Adapter(QMainWindow):

    def __init__(self):
        super().__init__()

        if sys.platform == "darwin":
            self.initUI(
                xComboBox=20, yComboBox=100, wComboBox=200, hComboBox=30,
                xConsol=10, yConsol=180, wConsol=480, hConsol=210,
                xLogo=280, yLogo=30, wLogo=200, hLogo=60,
                xVersionName=280, yVersionName=20, wVersionName=200, hVersionName=20,
                xLabelAvto=20, yLabelAvto=20,
                xLabelComoBox=20, yLabelComboBox=80,
                xClearButton=280, yClearButton=140, wClearButton=200, hClearButton=30, # 440, 200, 40, 30
                xSendButton=20, ySendButton=140, wSendButton=200, hSendButton=30,
                xStartThreadButton=20, yStartThreadButton=40, wStartThreadButton=100, hStartThreadButton=30,
                xStopThreadButton=120, yStopThreadButton=40, wStopThreadButton=100, hStopThreadButton=30,
                xSelf=300, ySelf=300, wSelf=500, hSelf=410, wFixedSelf=500, hFixedSelf=410,
            # Custom Tab
                xBitrateCombo=20, yBitrateCombo=100, wBitrateCombo=200, hBitrateCombo=30,
                xCanIdInput=20, yCanIdInput=160, wCanIdInput=40, hCanIdInput=30,
                xDlcCombo=60, yDlcCombo=160, wDlcCombo=60, hDlcCombo=35,  # Geometry for DlcCombo
                xDataInput1=120, yDataInput1=160, wDataInput1=40, hDataInput1=30,  # Geometry for DataInput1
                xDataInput2=160, yDataInput2=160, wDataInput2=40, hDataInput2=30,  # Geometry for DataInput2
                xDataInput3=200, yDataInput3=160, wDataInput3=40, hDataInput3=30,  # Geometry for DataInput3
                xDataInput4=240, yDataInput4=160, wDataInput4=40, hDataInput4=30,  # Geometry for DataInput4
                xDataInput5=280, yDataInput5=160, wDataInput5=40, hDataInput5=30,  # Geometry for DataInput5
                xDataInput6=320, yDataInput6=160, wDataInput6=40, hDataInput6=30,  # Geometry for DataInput6
                xDataInput7=360, yDataInput7=160, wDataInput7=40, hDataInput7=30,  # Geometry for DataInput7
                xDataInput8=400, yDataInput8=160, wDataInput8=40, hDataInput8=30,  # Geometry for DataInput8
            )
        elif sys.platform == "win32":
            self.initUI(
                xComboBox=20, yComboBox=100, wComboBox=200, hComboBox=30,
                xConsol=10, yConsol=180, wConsol=480, hConsol=210,
                xLogo=280, yLogo=30, wLogo=200, hLogo=60,
                xVersionName=280, yVersionName=20, wVersionName=200, hVersionName=20,
                xLabelAvto=20, yLabelAvto=20,
                xLabelComoBox=20, yLabelComboBox=80,
                xClearButton=280, yClearButton=140, wClearButton=200, hClearButton=30,
                xSendButton=20, ySendButton=140, wSendButton=200, hSendButton=30,
                xStartThreadButton=20, yStartThreadButton=40, wStartThreadButton=100, hStartThreadButton=30,
                xStopThreadButton=120, yStopThreadButton=40, wStopThreadButton=100, hStopThreadButton=30,
                xSelf=300, ySelf=300, wSelf=500, hSelf=410, wFixedSelf=500, hFixedSelf=410,
                # Custom Tab
                xBitrateCombo=20, yBitrateCombo=100, wBitrateCombo=200, hBitrateCombo=30,
                xCanIdInput=20, yCanIdInput=160, wCanIdInput=40, hCanIdInput=30,
                xDlcCombo=70, yDlcCombo=160, wDlcCombo=40, hDlcCombo=30,            # Geometry for DlcCombo
                xDataInput1=120, yDataInput1=160, wDataInput1=40, hDataInput1=30,  # Geometry for DataInput1
                xDataInput2=160, yDataInput2=160, wDataInput2=40, hDataInput2=30,  # Geometry for DataInput2
                xDataInput3=200, yDataInput3=160, wDataInput3=40, hDataInput3=30,  # Geometry for DataInput3
                xDataInput4=240, yDataInput4=160, wDataInput4=40, hDataInput4=30,  # Geometry for DataInput4
                xDataInput5=280, yDataInput5=160, wDataInput5=40, hDataInput5=30,  # Geometry for DataInput5
                xDataInput6=320, yDataInput6=160, wDataInput6=40, hDataInput6=30,  # Geometry for DataInput6
                xDataInput7=360, yDataInput7=160, wDataInput7=40, hDataInput7=30,  # Geometry for DataInput7
                xDataInput8=400, yDataInput8=160, wDataInput8=40, hDataInput8=30,  # Geometry for DataInput8
            )
        elif sys.platform == "linux" or sys.platform == "linux2":
            self.initUI(
                xComboBox=20, yComboBox=105, wComboBox=200, hComboBox=30,
                xConsol=10, yConsol=170, wConsol=480, hConsol=220,
                xLogo=280, yLogo=35, wLogo=200, hLogo=60,
                xVersionName=280, yVersionName=25, wVersionName=200, hVersionName=20,
                xLabelAvto=20, yLabelAvto=25,
                xLabelComoBox=20, yLabelComboBox=85,
                xClearButton=280, yClearButton=135, wClearButton=200, hClearButton=30,
                xSendButton=20, ySendButton=135, wSendButton=200, hSendButton=30,
                xStartThreadButton=20, yStartThreadButton=45, wStartThreadButton=100, hStartThreadButton=30,
                xStopThreadButton=120, yStopThreadButton=45, wStopThreadButton=100, hStopThreadButton=30,
                xSelf=300, ySelf=300, wSelf=500, hSelf=410, wFixedSelf=500, hFixedSelf=410
            )

    def initUI(self,
               xComboBox,yComboBox, wComboBox, hComboBox,               # Geometry for ComboBox
               xConsol, yConsol, wConsol, hConsol,                      # Geometry for Consol
               xLogo, yLogo, wLogo, hLogo,                              # Geometry for Logo
               xVersionName, yVersionName, wVersionName, hVersionName,  # Geometry for VersionName
               xLabelAvto, yLabelAvto,                                  # Geometry for LabelAvto
               xLabelComoBox, yLabelComboBox,                           # Geometry for LabelComboBox
               xClearButton, yClearButton, wClearButton, hClearButton,  # Geometry for ClearButton
               xSendButton, ySendButton, wSendButton, hSendButton,                              # Geometry for SendButton
               xStartThreadButton, yStartThreadButton, wStartThreadButton, hStartThreadButton,  # Geometry for StartThreadButton
               xStopThreadButton, yStopThreadButton, wStopThreadButton, hStopThreadButton,      # Geometry for StopThreadButton
               xSelf, ySelf, wSelf, hSelf, wFixedSelf, hFixedSelf,                              # Geometry for FixedSelf
               xBitrateCombo, yBitrateCombo, wBitrateCombo, hBitrateCombo,                      # Geometry for BitrateCombo
               xCanIdInput, yCanIdInput, wCanIdInput, hCanIdInput,                              # Geometry for CanIdInput
               xDlcCombo, yDlcCombo, wDlcCombo, hDlcCombo,                                      # Geometry for DlcCombo
               xDataInput1, yDataInput1, wDataInput1, hDataInput1,      # Geometry for DataInput1
               xDataInput2, yDataInput2, wDataInput2, hDataInput2,      # Geometry for DataInput2
               xDataInput3, yDataInput3, wDataInput3, hDataInput3,      # Geometry for DataInput3
               xDataInput4, yDataInput4, wDataInput4, hDataInput4,      # Geometry for DataInput4
               xDataInput5, yDataInput5, wDataInput5, hDataInput5,      # Geometry for DataInput5
               xDataInput6, yDataInput6, wDataInput6, hDataInput6,      # Geometry for DataInput6
               xDataInput7, yDataInput7, wDataInput7, hDataInput7,      # Geometry for DataInput7
               xDataInput8, yDataInput8, wDataInput8, hDataInput8,      # Geometry for DataInput8
               ):

        self.myclose = True  # myclose = True - закроете X и остановится фоновый поток
        self.combo_box = QComboBox(self)
        self.combo_box.activated.connect(self.onActivated_Combo_Box)
        self.consol = QTextBrowser(self)
        self.label_logo = QLabel(Var.LOGO, self)
        self.label_version_name = QLabel(Var.VERSION_NAME, self)
        self.label_avto = QLabel(Var.LABEL_AVTO, self)
        self.label_combo_box = QLabel(Var.LABEL_COMBO_BOX, self)
        self.clear_button = QPushButton(Var.NAME_CLEAR_BUTTON, self)
        self.current_auto = ""  # Polo/Octavia/Nissan/Mitsu...
        self.directory = ""
        self.fileName = ""

        # Start Status Bur
        self.statusBar()   # Add Status

        self.label_version_name.setStatusTip(Var.VERSION_INFO)

        # Menu "Exit"
        # exitAct = QAction(QIcon('exit.png'), '&Exit', self)
        # exitAct.setShortcut('Ctrl+Q')
        # exitAct.setStatusTip('Exit application')
        # exitAct.triggered.connect(qApp.quit)
        # Menu "Open File"
        # open_file = QAction(QIcon('open.png'), 'Open CAN-Dump', self)
        # open_file.setShortcut('Ctrl+O')
        # open_file.setStatusTip('Open new File')
        # open_file.triggered.connect(self.showDialog)
        # Menu custom command
        custom_command = QAction('[Custom command]', self)
        custom_command.setStatusTip('Custom command')
        custom_command.triggered.connect(self.customCommand)
        # Menu "Polo Car"
        polo_car = QAction('[Polo commands]', self)
        polo_car.setStatusTip('Polo commands')
        polo_car.triggered.connect(self.polo)
        # Menu "Octavia Car"
        octavia_car = QAction('[Octavia commands]', self)
        octavia_car.setStatusTip('Octavia commands')
        octavia_car.triggered.connect(self.octavia)
        # Menu "Nissan Car"
        nissan_car = QAction('[Nissan commands]', self)
        nissan_car.setStatusTip('Nissan commands')
        nissan_car.triggered.connect(self.nissan)
        # Menu "Mitsubishi Car"
        mitsubishi_car = QAction('[Mitsubishi commands]', self)
        mitsubishi_car.setStatusTip('Mitsubishi commands')
        mitsubishi_car.triggered.connect(self.mitsubishi)
        # Menu "Vesta & X-Ray Car"
        vesta_car = QAction('[Vesta and X-Ray commands]', self)
        vesta_car.setStatusTip('Vesta and X-Ray commands')
        vesta_car.triggered.connect(self.vesta)
        # Menu "Granta"
        granta_car = QAction('[Granta commands]', self)
        granta_car.setStatusTip('Granta commands')
        granta_car.triggered.connect(self.granta)
        arkana_car = QAction('[Arkana commands]', self)
        arkana_car.setStatusTip('Arkana commands')
        arkana_car.triggered.connect(self.arkana)
        # Can-Hacker CAN-Dump Play
        # logs_player = QAction('CAN-logs player', self)
        # logs_player.setStatusTip('CAN-logs player')
        # logs_player.triggered.connect(self.can_logs_player)
        # open_file = QAction(QIcon('open.png'), 'Open CAN-Dump', self)
        # open_file.setStatusTip('Open new File')
        # open_file.triggered.connect(self.showDialog)

        # Menu-Bar
        menubar = self.menuBar()
        # fileMenu = menubar.addMenu('&File')
        # fileMenu.addAction(exitAct)
        # fileMenu = menubar.addMenu('&Play CAN-Dump')
        # fileMenu.addAction(open_file)
        fileMenu = menubar.addMenu('&Custom')
        fileMenu.addAction(custom_command)
        fileMenu = menubar.addMenu('&Polo')
        fileMenu.addAction(polo_car)
        fileMenu = menubar.addMenu('&Octavia')
        fileMenu.addAction(octavia_car)
        fileMenu = menubar.addMenu('&Nissan')
        fileMenu.addAction(nissan_car)
        fileMenu = menubar.addMenu('&Mitsubishi')
        fileMenu.addAction(mitsubishi_car)
        fileMenu = menubar.addMenu('&Lada')
        fileMenu.addAction(vesta_car)
        fileMenu.addAction(granta_car)
        fileMenu = menubar.addMenu('&Arkana')
        fileMenu.addAction(arkana_car)
        # fileMenu = menubar.addMenu('&Logs player')
        # fileMenu.addAction(logs_player)
        # fileMenu.addAction(open_file)

        # Combo - box
        self.combo_box.setGeometry(xComboBox, yComboBox, wComboBox, hComboBox)
        self.combo_box.setStatusTip(Var.STATUS_COMBO_BOX)
        self.combo_box.setEnabled(False)
        # self.combo_box.activated[str].connect(self.onActivated_box)

        # Combo box for Vesta & Granta
        self.combo_box_1 = QComboBox(self)
        self.combo_box_1.hide()

        self.combo_box_2 = QComboBox(self)
        self.combo_box_2.hide()

        self.combo_box_3 = QComboBox(self)
        self.combo_box_3.hide()

        # Add new combo box button
        self.addComboBoxButton = QPushButton('+', self)
        # self.addComboBoxButton.clicked.connect(self.addNewCombo)
        self.addComboBoxButton.setStatusTip('Add a command')
        self.addComboBoxButton.setGeometry(210, 100, 40, 30)
        self.addComboBoxButton.hide()
        self.addComboBoxButton.setEnabled(False)

        # Hide new combo box button
        self.hideComboBoxButton = QPushButton('-', self)
        # self.hideComboBoxButton.clicked.connect(self.hideNewCombo)
        self.hideComboBoxButton.setStatusTip('Delete a command')
        self.hideComboBoxButton.setGeometry(240, 100, 40, 30)
        self.hideComboBoxButton.hide()

        # CheckBox for Driver Door (open/close)
        self.open_drivers_door_CheckBox = QCheckBox('Open driver door', self)
        self.open_drivers_door_CheckBox.setGeometry(20, 150, 130, 30)
        self.open_drivers_door_CheckBox.hide()

        # CheckBox for Passenger Door (open/close)
        self.open_passenger_door_CheckBox = QCheckBox('Open passenger door', self)
        self.open_passenger_door_CheckBox.setGeometry(150, 150, 170, 30)
        self.open_passenger_door_CheckBox.hide()

        # CheckBox for Back Doors (open/close)
        self.open_back_doors_CheckBox = QCheckBox('Open back doors', self)
        self.open_back_doors_CheckBox.setGeometry(20, 170, 130, 30)
        self.open_back_doors_CheckBox.hide()

        # CheckBox for Hood (open/close)
        self.open_hood_CheckBox = QCheckBox('Open hood', self)
        self.open_hood_CheckBox.setGeometry(20, 130, 130, 30)
        self.open_hood_CheckBox.hide()

        # CheckBox for Trunk (open/close)
        self.open_trunk_CheckBox = QCheckBox('Open trunk', self)
        self.open_trunk_CheckBox.setGeometry(150, 130, 130, 30)
        self.open_trunk_CheckBox.hide()

        # # Hide consol button
        # self.hideConsolButton = QPushButton('+', self)
        # self.hideConsolButton.clicked.connect(self.hideConsol)
        # self.hideConsolButton.setStatusTip('Show/Hide consol')
        # self.hideConsolButton.setGeometry(440, 200, 40, 30)
        # self.hideConsolButton.show()

        # Text Browser
        self.consol.setGeometry(xConsol, yConsol, wConsol, hConsol)
        # self.consol.hide()self.consol.hide()
        self.consol.show()

        # Label Logo
        self.label_logo.setGeometry(xLogo, yLogo, wLogo, hLogo)

        # Label Verion name
        self.label_version_name.setGeometry(xVersionName, yVersionName, wVersionName, hVersionName)

        # Label Avto
        self.label_avto.resize(self.label_avto.sizeHint())
        self.label_avto.move(xLabelAvto, yLabelAvto)

        # Label Combo - box
        self.label_combo_box.resize(self.label_combo_box.sizeHint())
        self.label_combo_box.move(xLabelComoBox, yLabelComboBox)

        # Clear button
        self.clear_button.clicked.connect(self.click_clear_button)
        self.clear_button.setStatusTip(Var.STATUS_CLEAR_BUTTON)
        self.clear_button.setGeometry(xClearButton, yClearButton, wClearButton, hClearButton)
        # self.clear_button.move(250, 100)
        self.clear_button.setEnabled(False)
        self.clear_button.show()

        # Send button
        self.send_button = QPushButton(Var.NAME_SEND_BUTTON, self)
        self.send_button.clicked.connect(self.click_send_button)
        self.send_button.setStatusTip(Var.STATUS_SEND_BUTTON)
        self.send_button.setGeometry(xSendButton, ySendButton, wSendButton, hSendButton)
        self.send_button.show()
        self.send_button.setEnabled(False)

        # Start thread button
        self.start_thread_button = QPushButton(Var.NAME_START_THREAD_BUTTON, self)
        self.start_thread_button.clicked.connect(self.click_start_thread_button)
        self.start_thread_button.setStatusTip(Var.STATUS_START_THREAD_BUTTON)
        self.start_thread_button.setGeometry(xStartThreadButton, yStartThreadButton, wStartThreadButton, hStartThreadButton)
        self.start_thread_button.show()
        self.start_thread_button.setEnabled(False)

        # Stop thread button
        self.stop_thread_button = QPushButton(Var.NAME_STOP_THREAD_BUTTON, self)
        self.stop_thread_button.clicked.connect(self.click_stop_thread_button)
        self.stop_thread_button.setStatusTip(Var.STATUS_STOP_THREAD_BUTTON)
        self.stop_thread_button.setGeometry(xStopThreadButton, yStopThreadButton, wStopThreadButton, hStopThreadButton)
        self.stop_thread_button.show()
        self.stop_thread_button.setEnabled(False)

        self.setGeometry(xSelf, ySelf, wSelf, hSelf)  # (X-position,Y-position,X-size,Y-size)
        self.setFixedSize(wFixedSelf, hFixedSelf)
        self.setWindowTitle(Var.WINDOW_TITLE)
        self.setWindowIcon(QIcon('icon.png'))  # Add Icon

        # Custom Combobox
        self.bitrateComboBox = QComboBox(self)
        self.bitrateComboBox.setGeometry(xBitrateCombo, yBitrateCombo, wBitrateCombo, hBitrateCombo)
        self.bitrateComboBox.setStatusTip('Bitrate')
        self.bitrateComboBox.addItem("250000 (bit/s)")
        self.bitrateComboBox.addItem("500000 (bit/s)")
        self.bitrateComboBox.addItem("1000000 (bit/s)")
        self.bitrateComboBox.setEnabled(False)
        self.bitrateComboBox.repaint()
        self.bitrateComboBox.hide()

        # [ID] Input
        self.canIdInput = QLineEdit(self)
        self.canIdInput.setAlignment(QtCore.Qt.AlignCenter)
        self.canIdInput.setGeometry(xCanIdInput, yCanIdInput, wCanIdInput, hCanIdInput)
        self.canIdInput.setStatusTip('CAN ID')
        self.canIdInput.setEnabled(False)
        self.canIdInput.setText("00")
        self.canIdInput.setMaxLength(3)
        self.canIdInput.repaint()
        self.canIdInput.hide()

        # [Dlc] ComboBox
        self.dlcCombo = QComboBox(self)
        self.dlcCombo.activated.connect(self.onActivated_dlc_Combo_Box)
        self.dlcCombo.setGeometry(xDlcCombo, yDlcCombo, wDlcCombo, hDlcCombo)
        self.dlcCombo.setStatusTip('DLC')
        self.dlcCombo.addItem("8")
        self.dlcCombo.addItem("7")
        self.dlcCombo.addItem("6")
        self.dlcCombo.addItem("5")
        self.dlcCombo.addItem("4")
        self.dlcCombo.addItem("3")
        self.dlcCombo.addItem("2")
        self.dlcCombo.addItem("1")
        # self.dlcCombo.activated.connect(self.setDataFromDlc)
        self.dlcCombo.setEnabled(False)
        self.dlcCombo.show()
        self.dlcCombo.repaint()
        self.dlcCombo.hide()

        # [Data] input
        self.dataInput1 = QLineEdit(self)
        self.dataInput1.setAlignment(QtCore.Qt.AlignCenter)     # Text Centre
        self.dataInput1.setGeometry(xDataInput1, yDataInput1, wDataInput1, hDataInput1)
        self.dataInput1.setStatusTip('Data')
        self.dataInput1.setText("00")
        self.dataInput1.setMaxLength(2)
        self.dataInput1.setEnabled(False)
        self.dataInput2 = QLineEdit(self)
        self.dataInput2.setAlignment(QtCore.Qt.AlignCenter)     # Text Centre
        self.dataInput2.setGeometry(xDataInput2, yDataInput2, wDataInput2, hDataInput2)
        self.dataInput2.setStatusTip('Data')
        self.dataInput2.setText("00")
        self.dataInput2.setMaxLength(2)
        self.dataInput2.setEnabled(False)
        self.dataInput3 = QLineEdit(self)
        self.dataInput3.setAlignment(QtCore.Qt.AlignCenter)     # Text Centre
        self.dataInput3.setGeometry(xDataInput3, yDataInput3, wDataInput3, hDataInput3)
        self.dataInput3.setStatusTip('Data')
        self.dataInput3.setText("00")
        self.dataInput3.setMaxLength(2)
        self.dataInput3.setEnabled(False)
        self.dataInput4 = QLineEdit(self)
        self.dataInput4.setAlignment(QtCore.Qt.AlignCenter)     # Text Centre
        self.dataInput4.setGeometry(xDataInput4, yDataInput4, wDataInput4, hDataInput4)
        self.dataInput4.setStatusTip('Data')
        self.dataInput4.setText("00")
        self.dataInput4.setMaxLength(2)
        self.dataInput4.setEnabled(False)
        self.dataInput5 = QLineEdit(self)
        self.dataInput5.setAlignment(QtCore.Qt.AlignCenter)     # Text Centre
        self.dataInput5.setGeometry(xDataInput5, yDataInput5, wDataInput5, hDataInput5)
        self.dataInput5.setStatusTip('Data')
        self.dataInput5.setText("00")
        self.dataInput5.setMaxLength(4)
        self.dataInput5.setEnabled(False)
        self.dataInput6 = QLineEdit(self)
        self.dataInput6.setAlignment(QtCore.Qt.AlignCenter)     # Text Centre
        self.dataInput6.setGeometry(xDataInput6, yDataInput6, wDataInput6, hDataInput6)
        self.dataInput6.setStatusTip('Data')
        self.dataInput6.setText("00")
        self.dataInput6.setMaxLength(2)
        self.dataInput6.setEnabled(False)
        self.dataInput7 = QLineEdit(self)
        self.dataInput7.setAlignment(QtCore.Qt.AlignCenter)     # Text Centre
        self.dataInput7.setGeometry(xDataInput7, yDataInput7, wDataInput7, hDataInput7)
        self.dataInput7.setStatusTip('Data')
        self.dataInput7.setText("00")
        self.dataInput7.setMaxLength(2)
        self.dataInput7.setEnabled(False)
        self.dataInput8 = QLineEdit(self)
        self.dataInput8.setAlignment(QtCore.Qt.AlignCenter)     # Text Centre
        self.dataInput8.setGeometry(xDataInput8, yDataInput8, wDataInput8, hDataInput8)
        self.dataInput8.setStatusTip('Data')
        self.dataInput8.setText("00")
        self.dataInput8.setMaxLength(2)
        self.dataInput8.setEnabled(False)
        self.dataInput1.hide()
        self.dataInput2.hide()
        self.dataInput3.hide()
        self.dataInput4.hide()
        self.dataInput5.hide()
        self.dataInput6.hide()
        self.dataInput7.hide()
        self.dataInput8.hide()

        # ID LABEL
        self.IDLabel = QLabel(self)
        self.IDLabel.setText("ID")
        self.IDLabel.setGeometry(35, 135, 40, 30)
        self.IDLabel.hide()
        # DLC LABEL
        self.DLCLabel = QLabel(self)
        self.DLCLabel.setText("DLC")
        self.DLCLabel.setGeometry(80, 135, 40, 30)
        self.DLCLabel.hide()
        # DATA LABEL
        self.DataLabel = QLabel(self)
        self.DataLabel.setText("DATA")
        self.DataLabel.setGeometry(270, 135, 40, 30)
        self.DataLabel.hide()

        # Theme buttons
        #self.themeButtonSwitch = QPushButton('Blue', self)
        #self.themeButtonSwitch.setCheckable(True)
        #self.themeButtonSwitch.move(250, 20)
        #self.themeButtonSwitch.clicked.connect(self.switchTheme)

        # Info lable
        self.infoLable = QLabel(self)
        self.infoLable.setGeometry(280, 100, 200, 30)
        self.infoLable.setText("""
        <head>
        <style>
        * {
        text-align: right;
        }
        </head>
        </style>
        <body>
        <div>Thread is stopped</div>
        </body>
        """)

        #Test box
        #self.box = QFrame(self)
        #self.box.setGeometry(250, 50, 20, 20)
        #self.box.setStyleSheet("QFrame { background-color: Red }")


        self.show()

    #----------------------------------------------------------
    #           "CUSTOM"
    # ---------------------------------------------------------
    def customCommand(self):
        if Var.thread_flag == 'on':
            self.stop_thread_info()
            Var.thread_flag = 'off'
        self.start_thread_button.setEnabled(True)
        self.start_thread_button.repaint()
        self.stop_thread_button.setEnabled(False)
        self.stop_thread_button.repaint()
        self.combo_box.setEnabled(False)
        self.combo_box.repaint()
        self.send_button.setEnabled(False)
        self.send_button.repaint()
        self.bitrateComboBox.setEnabled(True)
        self.bitrateComboBox.repaint()
        self.dlcCombo.setEnabled(False)
        self.dlcCombo.setCurrentText('8')
        self.dlcCombo.repaint()
        self.clear_button.setEnabled(False)
        self.clear_button.repaint()
        self.combo_box.hide()
        self.combo_box_1.hide()
        self.combo_box_2.hide()
        self.combo_box_3.hide()
        self.addComboBoxButton.hide()
        self.hideComboBoxButton.hide()
        self.label_combo_box.setText(Var.LABEL_COMBO_BOX_CUSTOM)
        #
        self.label_avto.setText(Var.LABEL_COMBO_BOX_CUSTOM)
        self.label_avto.repaint()
        self.label_combo_box.setText(Var.STATUS_START_THREAD_BUTTON)
        self.label_combo_box.repaint()
        self.bitrateComboBox.show()
        self.canIdInput.show()
        self.canIdInput.setEnabled(False)
        self.dlcCombo.show()
        self.dataInput1.show()
        self.dataInput1.setEnabled(False)
        self.dataInput2.show()
        self.dataInput2.setEnabled(False)
        self.dataInput3.show()
        self.dataInput3.setEnabled(False)
        self.dataInput4.show()
        self.dataInput4.setEnabled(False)
        self.dataInput5.show()
        self.dataInput5.setEnabled(False)
        self.dataInput6.show()
        self.dataInput6.setEnabled(False)
        self.dataInput7.show()
        self.dataInput7.setEnabled(False)
        self.dataInput8.show()
        self.dataInput8.setEnabled(False)
        # Move Buttons
        self.start_thread_button.move(20, 100)
        self.stop_thread_button.move(120, 100)
        self.bitrateComboBox.move(20, 40)
        # Hide CheckBoxs
        self.open_drivers_door_CheckBox.hide()
        self.open_trunk_CheckBox.hide()
        self.open_back_doors_CheckBox.hide()
        self.open_hood_CheckBox.hide()
        self.open_passenger_door_CheckBox.hide()
        #
        self.current_auto = "Custom command"
        # Text Browser Geomerty
        self.consol.setGeometry(10, 240, 480, 150)
        self.consol.repaint()
        # Labels
        self.IDLabel.show()
        self.DLCLabel.show()
        self.DataLabel.show()
        self.send_button.move(20, 200)
        self.clear_button.move(280, 200)
        # self.send_button.setGeometry(xSendButton_Custom, ySendButton_Custom, wSendButton_Custom, hSendButton_Custom)

    #----------------------------------------------------------
    #           "POLO"
    # ---------------------------------------------------------
    def polo(self):
        if Var.thread_flag == "on":
            self.stop_thread_info()
            Var.thread_flag = "off"  # Thread OFF
        self.label_combo_box.setText(Var.LABEL_COMBO_BOX)
        self.start_thread_button.setEnabled(True)
        self.start_thread_button.repaint()
        self.stop_thread_button.setEnabled(False)
        self.stop_thread_button.repaint()
        self.combo_box.setEnabled(False)
        self.combo_box.repaint()
        self.send_button.setEnabled(False)
        self.send_button.repaint()
        self.clear_button.setEnabled(False)
        self.clear_button.repaint()
        self.bitrateComboBox.hide()
        self.canIdInput.hide()
        self.dlcCombo.hide()
        self.dataInput1.hide()
        self.dataInput2.hide()
        self.dataInput3.hide()
        self.dataInput4.hide()
        self.dataInput5.hide()
        self.dataInput6.hide()
        self.dataInput7.hide()
        self.dataInput8.hide()
        #
        # Hide CheckBoxs
        self.open_drivers_door_CheckBox.hide()
        self.open_trunk_CheckBox.hide()
        self.open_back_doors_CheckBox.hide()
        self.open_hood_CheckBox.hide()
        self.open_passenger_door_CheckBox.hide()
        #
        self.combo_box.show()
        self.combo_box.clear()
        self.combo_box.addItem("Volume Up")
        self.combo_box.addItem("Volume Down")
        self.combo_box.addItem("Next Track")
        self.combo_box.addItem("Previous Track")
        self.combo_box.addItem("Phone")
        self.combo_box.addItem("Voice")
        self.combo_box.addItem("Mute")
        self.combo_box.addItem("Reversing On")
        self.combo_box.addItem("Reversing Off")
        self.combo_box.addItem("Lighting on")
        self.combo_box.addItem("Lighting off")
        self.combo_box.repaint()
        self.combo_box_1.setEnabled(False)
        self.combo_box_1.repaint()
        self.combo_box_2.setEnabled(False)
        self.combo_box_2.repaint()
        self.combo_box_3.setEnabled(False)
        self.combo_box_3.repaint()
        self.addComboBoxButton.setEnabled(False)
        self.addComboBoxButton.repaint()
        self.hideComboBoxButton.setEnabled(False)
        self.hideComboBoxButton.repaint()
        # Move Buttons start/stop
        self.start_thread_button.move(20, 40)
        self.stop_thread_button.move(120, 40)
        self.bitrateComboBox.move(20, 100)
        # Send Button
        self.send_button.move(20, 135)
        # Clear Button
        self.clear_button.move(280, 135)
        # Text Browser Geomerty
        self.consol.setGeometry(10, 180, 480, 210)
        self.consol.repaint()
        # Labels
        self.IDLabel.hide()
        self.DLCLabel.hide()
        self.DataLabel.hide()
        self.label_avto.setText(Var.LABEL_CURRENT_AUTO + 'WV Polo')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.label_avto.repaint()
        self.current_auto = "Polo"

    #----------------------------------------------------------
    #           "OCTAVIA"
    # ---------------------------------------------------------
    def octavia(self):
        if Var.thread_flag == "on":
            self.stop_thread_info()
            Var.thread_flag = "off"  # Thread OFF
        self.label_combo_box.setText(Var.LABEL_COMBO_BOX)
        self.start_thread_button.setEnabled(True)
        self.start_thread_button.repaint()
        self.stop_thread_button.setEnabled(False)
        self.stop_thread_button.repaint()
        self.combo_box.setEnabled(False)
        self.combo_box.repaint()
        self.send_button.setEnabled(False)
        self.send_button.repaint()
        self.clear_button.setEnabled(False)
        self.clear_button.repaint()
        self.bitrateComboBox.hide()
        self.canIdInput.hide()
        self.dlcCombo.hide()
        self.dataInput1.hide()
        self.dataInput2.hide()
        self.dataInput3.hide()
        self.dataInput4.hide()
        self.dataInput5.hide()
        self.dataInput6.hide()
        self.dataInput7.hide()
        self.dataInput8.hide()
        #
        # Hide CheckBoxs
        self.open_drivers_door_CheckBox.hide()
        self.open_trunk_CheckBox.hide()
        self.open_back_doors_CheckBox.hide()
        self.open_hood_CheckBox.hide()
        self.open_passenger_door_CheckBox.hide()
        #
        self.combo_box.show()
        self.combo_box.clear()
        self.combo_box.addItem("Volume Up")
        self.combo_box.addItem("Volume Down")
        self.combo_box.addItem("Next Track")
        self.combo_box.addItem("Previous Track")
        self.combo_box.addItem("Next Track long press")
        self.combo_box.addItem("Previous Track long press")
        self.combo_box.addItem("Phone")
        self.combo_box.addItem("Voice")
        self.combo_box.addItem("Mute")
        self.combo_box.repaint()
        self.combo_box_1.setEnabled(False)
        self.combo_box_1.repaint()
        self.combo_box_2.setEnabled(False)
        self.combo_box_2.repaint()
        self.combo_box_3.setEnabled(False)
        self.combo_box_3.repaint()
        self.addComboBoxButton.setEnabled(False)
        self.addComboBoxButton.repaint()
        self.hideComboBoxButton.setEnabled(False)
        self.hideComboBoxButton.repaint()
        # Move Buttons start/stop
        self.start_thread_button.move(20, 40)
        self.stop_thread_button.move(120, 40)
        self.bitrateComboBox.move(20, 100)
        # Send Button
        self.send_button.move(20, 135)
        # Clear Button
        self.clear_button.move(280, 135)
        # Text Browser Geomerty
        self.consol.setGeometry(10, 180, 480, 210)
        self.consol.repaint()
        # Labels
        self.IDLabel.hide()
        self.DLCLabel.hide()
        self.DataLabel.hide()
        self.label_avto.setText(Var.LABEL_CURRENT_AUTO + 'Octavia')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.label_avto.repaint()
        self.current_auto = "Octavia"

    #----------------------------------------------------------
    #           "NISSAN"
    # ---------------------------------------------------------
    def nissan(self):
        if Var.thread_flag == "on":
            self.stop_thread_info()
            Var.thread_flag = "off"  # Thread OFF
        self.label_combo_box.setText(Var.LABEL_COMBO_BOX)
        self.start_thread_button.setEnabled(True)
        self.start_thread_button.repaint()
        self.stop_thread_button.setEnabled(False)
        self.stop_thread_button.repaint()
        self.combo_box.setEnabled(False)
        self.combo_box.repaint()
        self.send_button.setEnabled(False)
        self.send_button.repaint()
        self.clear_button.setEnabled(False)
        self.clear_button.repaint()
        self.bitrateComboBox.hide()
        self.canIdInput.hide()
        self.dlcCombo.hide()
        self.dataInput1.hide()
        self.dataInput2.hide()
        self.dataInput3.hide()
        self.dataInput4.hide()
        self.dataInput5.hide()
        self.dataInput6.hide()
        self.dataInput7.hide()
        self.dataInput8.hide()
        #
        # Hide CheckBoxs
        self.open_drivers_door_CheckBox.hide()
        self.open_trunk_CheckBox.hide()
        self.open_back_doors_CheckBox.hide()
        self.open_hood_CheckBox.hide()
        self.open_passenger_door_CheckBox.hide()
        #
        self.combo_box.show()
        self.combo_box.clear()
        self.combo_box.addItem("Volume Up")
        self.combo_box.addItem("Volume Down")
        self.combo_box.addItem("Next Track")
        self.combo_box.addItem("Previous Track")
        self.combo_box.addItem("Hang Up")
        self.combo_box.addItem("Mode")# "Voice On"
        self.combo_box.addItem("Phone/Voice Short Press")
        self.combo_box.addItem("Phone/Voice Long Press")
        self.combo_box.repaint()
        self.combo_box_1.setEnabled(False)
        self.combo_box_1.repaint()
        self.combo_box_2.setEnabled(False)
        self.combo_box_2.repaint()
        self.combo_box_3.setEnabled(False)
        self.combo_box_3.repaint()
        self.addComboBoxButton.setEnabled(False)
        self.addComboBoxButton.repaint()
        self.hideComboBoxButton.setEnabled(False)
        self.hideComboBoxButton.repaint()
        # Move Buttons start/stop
        self.start_thread_button.move(20, 40)
        self.stop_thread_button.move(120, 40)
        self.bitrateComboBox.move(20, 100)
        # Send Button
        self.send_button.move(20, 135)
        # Clear Button
        self.clear_button.move(280, 135)
        # Text Browser Geomerty
        self.consol.setGeometry(10, 180, 480, 210)
        self.consol.repaint()
        # Labels
        self.IDLabel.hide()
        self.DLCLabel.hide()
        self.DataLabel.hide()
        self.label_avto.setText(Var.LABEL_CURRENT_AUTO + 'Nissan')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.label_avto.repaint()
        self.current_auto = "Nissan"

    #----------------------------------------------------------
    #           "MITSUBISHI"
    # ---------------------------------------------------------
    def mitsubishi(self):
        if Var.thread_flag == "on":
            self.stop_thread_info()
            Var.thread_flag = "off"  # Thread OFF
        self.label_combo_box.setText(Var.LABEL_COMBO_BOX)
        self.start_thread_button.setEnabled(True)
        self.start_thread_button.repaint()
        self.stop_thread_button.setEnabled(False)
        self.stop_thread_button.repaint()
        self.combo_box.setEnabled(False)
        self.combo_box.repaint()
        self.send_button.setEnabled(False)
        self.send_button.repaint()
        self.clear_button.setEnabled(False)
        self.clear_button.repaint()
        self.bitrateComboBox.hide()
        self.canIdInput.hide()
        self.dlcCombo.hide()
        self.dataInput1.hide()
        self.dataInput2.hide()
        self.dataInput3.hide()
        self.dataInput4.hide()
        self.dataInput5.hide()
        self.dataInput6.hide()
        self.dataInput7.hide()
        self.dataInput8.hide()
        #
        # Hide CheckBoxs
        self.open_drivers_door_CheckBox.hide()
        self.open_trunk_CheckBox.hide()
        self.open_back_doors_CheckBox.hide()
        self.open_hood_CheckBox.hide()
        self.open_passenger_door_CheckBox.hide()
        #
        self.combo_box.show()
        self.combo_box.clear()
        # Air Conditioning
        self.combo_box.addItem("AC On")
        self.combo_box.addItem("AC Off")
        self.combo_box.addItem("Activation Dual fun mode")
        self.combo_box.addItem("Fun Auto mode on")
        self.combo_box.addItem("Fun Auto mode off")
        self.combo_box.addItem("Left Tem. regulation up")
        self.combo_box.addItem("Right Tem. regulation up")
        self.combo_box.addItem("Ventilator Power up")
        # Fan Direction
        self.combo_box.addItem("Face")
        self.combo_box.addItem("Face & Foot")
        self.combo_box.addItem("Foot")
        self.combo_box.addItem("Foot & Windscreen")
        # Rear Window Heating
        self.combo_box.addItem("Rear Window Heating On")
        self.combo_box.addItem("Rear Window Heating Off")
        # Recirculation
        self.combo_box.addItem("Recirculation Cabin")
        self.combo_box.addItem("Recirculation Outside")
        # Reversing On/Off
        self.combo_box.addItem("Reversing On")
        self.combo_box.addItem("Reversing Off")
        # Windscreen Heating
        self.combo_box.addItem("Windscreen Heating On")
        self.combo_box.addItem("Windscreen Heating Off")
        self.combo_box.repaint()
        self.combo_box_1.setEnabled(False)
        self.combo_box_1.repaint()
        self.combo_box_2.setEnabled(False)
        self.combo_box_2.repaint()
        self.combo_box_3.setEnabled(False)
        self.combo_box_3.repaint()
        self.addComboBoxButton.setEnabled(False)
        self.addComboBoxButton.repaint()
        self.hideComboBoxButton.setEnabled(False)
        self.hideComboBoxButton.repaint()
        # Move Buttons start/stop
        self.start_thread_button.move(20, 40)
        self.stop_thread_button.move(120, 40)
        self.bitrateComboBox.move(20, 100)
        # Send Button
        self.send_button.move(20, 135)
        # Clear Button
        self.clear_button.move(280, 135)
        # Text Browser Geomerty
        self.consol.setGeometry(10, 180, 480, 210)
        self.consol.repaint()
        # Labels
        self.IDLabel.hide()
        self.DLCLabel.hide()
        self.DataLabel.hide()
        self.label_avto.setText(Var.LABEL_CURRENT_AUTO + 'Mitsubishi')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.label_avto.repaint()
        self.current_auto = "Mitsubishi"

    #----------------------------------------------------------
    #           "VESTA"
    # ---------------------------------------------------------
    def vesta(self):
        if Var.thread_flag == "on":
            self.stop_thread_info()
            Var.thread_flag = "off"  # Thread OFF
        self.label_combo_box.setText(Var.LABEL_COMBO_BOX)
        self.start_thread_button.setEnabled(True)
        self.start_thread_button.repaint()
        self.stop_thread_button.setEnabled(False)
        self.stop_thread_button.repaint()
        self.combo_box.setEnabled(False)
        self.combo_box.repaint()
        self.send_button.setEnabled(False)
        self.send_button.repaint()
        self.clear_button.setEnabled(False)
        self.clear_button.repaint()
        self.bitrateComboBox.hide()
        self.canIdInput.hide()
        self.dlcCombo.hide()
        self.dataInput1.hide()
        self.dataInput2.hide()
        self.dataInput3.hide()
        self.dataInput4.hide()
        self.dataInput5.hide()
        self.dataInput6.hide()
        self.dataInput7.hide()
        self.dataInput8.hide()
        #
        # Hide CheckBoxs
        self.open_drivers_door_CheckBox.hide()
        self.open_trunk_CheckBox.hide()
        self.open_back_doors_CheckBox.hide()
        self.open_hood_CheckBox.hide()
        self.open_passenger_door_CheckBox.hide()
        #
        self.combo_box.show()
        self.combo_box.clear()
        self.combo_box.addItem("Handbrake (Unblock AA)")
        self.combo_box.addItem("Speed_5_km_h (Unblock Browser)")
        self.combo_box.addItem("Speed_15_km_h (Block Browser)")
        self.combo_box.addItem("Reversing On (Camera On)")
        self.combo_box.addItem("Reversing Off (Camera Off)")
        self.combo_box.addItem("Parktronic zero position")
        self.combo_box.addItem("Parktronic leftmost position")
        self.combo_box.addItem("Parktronic rightmost position")
        self.combo_box.addItem("External Sensor Temperature -18")
        self.combo_box.addItem("External Sensor Temperature 0")
        self.combo_box.addItem("External Sensor Temperature +12")
        self.combo_box.addItem("Open Hood/Trunk/Doors")
        self.combo_box.repaint()
        # self.combo_box_1.setEnabled(True)
        # self.combo_box_1.repaint()
        # self.combo_box_2.setEnabled(True)
        # self.combo_box_2.repaint()
        # self.combo_box_3.setEnabled(True)
        # self.combo_box_3.repaint()
        # self.addComboBoxButton.setEnabled(False)
        # self.addComboBoxButton.show()
        # self.addComboBoxButton.repaint()
        # self.hideComboBoxButton.setEnabled(False)
        # self.hideComboBoxButton.repaint
        # Move Buttons start/stop
        self.start_thread_button.move(20, 40)
        self.stop_thread_button.move(120, 40)
        self.bitrateComboBox.move(20, 100)
        # Send Button
        self.send_button.move(20, 135)
        # Clear Button
        self.clear_button.move(280, 135)
        # Text Browser Geomerty
        self.consol.setGeometry(10, 180, 480, 210)
        self.consol.repaint()
        # Labels
        self.IDLabel.hide()
        self.DLCLabel.hide()
        self.DataLabel.hide()
        self.label_avto.setText(Var.LABEL_CURRENT_AUTO + 'Vesta & X-Ray')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.label_avto.repaint()
        self.current_auto = "Vesta"

    #----------------------------------------------------------
    #               "GRANTA"
    # ---------------------------------------------------------
    def granta(self):
        if Var.thread_flag == "on":
            self.stop_thread_info()
            Var.thread_flag = "off"  # Thread OFF
        self.label_combo_box.setText(Var.LABEL_COMBO_BOX)
        self.start_thread_button.setEnabled(True)
        self.start_thread_button.repaint()
        self.stop_thread_button.setEnabled(False)
        self.stop_thread_button.repaint()
        self.combo_box.setEnabled(False)
        self.combo_box.repaint()
        self.send_button.setEnabled(False)
        self.send_button.repaint()
        self.clear_button.setEnabled(False)
        self.clear_button.repaint()
        self.bitrateComboBox.hide()
        self.canIdInput.hide()
        self.dlcCombo.hide()
        self.dataInput1.hide()
        self.dataInput2.hide()
        self.dataInput3.hide()
        self.dataInput4.hide()
        self.dataInput5.hide()
        self.dataInput6.hide()
        self.dataInput7.hide()
        self.dataInput8.hide()
        #
        # Hide CheckBoxs
        self.open_drivers_door_CheckBox.hide()
        self.open_trunk_CheckBox.hide()
        self.open_back_doors_CheckBox.hide()
        self.open_hood_CheckBox.hide()
        self.open_passenger_door_CheckBox.hide()
        #
        self.combo_box.show()
        self.combo_box.clear()
        self.combo_box.addItem("Handbrake (Unblock AA)")
        self.combo_box.addItem("Speed_5_km_h (Unblock Browser)")
        self.combo_box.addItem("Speed_15_km_h (Block Browser)")
        self.combo_box.addItem("External Sensor Temperature -18")
        self.combo_box.addItem("External Sensor Temperature 0")
        self.combo_box.addItem("External Sensor Temperature +12")
        self.combo_box.repaint()
        self.combo_box_1.setEnabled(False)
        self.combo_box_1.repaint()
        self.combo_box_2.setEnabled(False)
        self.combo_box_2.repaint()
        self.combo_box_3.setEnabled(False)
        self.combo_box_3.repaint()
        self.addComboBoxButton.setEnabled(False)
        self.addComboBoxButton.repaint()
        self.hideComboBoxButton.setEnabled(False)
        self.hideComboBoxButton.repaint()
        # Move Buttons start/stop
        self.start_thread_button.move(20, 40)
        self.stop_thread_button.move(120, 40)
        self.bitrateComboBox.move(20, 100)
        # Send Button
        self.send_button.move(20, 135)
        # Clear Button
        self.clear_button.move(280, 135)
        # Text Browser Geomerty
        self.consol.setGeometry(10, 180, 480, 210)
        self.consol.repaint()
        # Labels
        self.IDLabel.hide()
        self.DLCLabel.hide()
        self.DataLabel.hide()
        self.label_avto.setText(Var.LABEL_CURRENT_AUTO + 'Granta')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.label_avto.repaint()
        self.current_auto = "Granta"

    #----------------------------------------------------------
    #           "ARKANA"
    # ---------------------------------------------------------
    def arkana(self):
        if Var.thread_flag == "on":
            self.stop_thread_info()
            Var.thread_flag = "off"  # Thread OFF
        self.label_combo_box.setText(Var.LABEL_COMBO_BOX)
        self.start_thread_button.setEnabled(True)
        self.start_thread_button.repaint()
        self.stop_thread_button.setEnabled(False)
        self.stop_thread_button.repaint()
        self.combo_box.setEnabled(False)
        self.combo_box.repaint()
        self.combo_box_1.setEnabled(False)
        self.combo_box_1.repaint()
        self.combo_box_2.setEnabled(False)
        self.combo_box_2.repaint()
        self.combo_box_3.setEnabled(False)
        self.combo_box_3.repaint()
        self.addComboBoxButton.setEnabled(False)
        self.addComboBoxButton.repaint()
        self.hideComboBoxButton.setEnabled(False)
        self.hideComboBoxButton.repaint()
        self.send_button.setEnabled(False)
        self.send_button.repaint()
        self.clear_button.setEnabled(False)
        self.clear_button.repaint()
        self.bitrateComboBox.hide()
        self.canIdInput.hide()
        self.dlcCombo.hide()
        self.dataInput1.hide()
        self.dataInput2.hide()
        self.dataInput3.hide()
        self.dataInput4.hide()
        self.dataInput5.hide()
        self.dataInput6.hide()
        self.dataInput7.hide()
        self.dataInput8.hide()
        #
        # Hide CheckBoxs
        self.open_drivers_door_CheckBox.hide()
        self.open_trunk_CheckBox.hide()
        self.open_back_doors_CheckBox.hide()
        self.open_hood_CheckBox.hide()
        self.open_passenger_door_CheckBox.hide()
        #
        self.combo_box.show()
        self.combo_box.clear()
        self.combo_box.addItem("Voice")
        self.combo_box.addItem("Handbrake (Unblock AA)")
        self.combo_box.repaint()
        # Move Buttons start/stop
        self.start_thread_button.move(20, 40)
        self.stop_thread_button.move(120, 40)
        self.bitrateComboBox.move(20, 100)
        # Send Button
        self.send_button.move(20, 135)
        # Clear Button
        self.clear_button.move(280, 135)
        # Text Browser Geomerty
        self.consol.setGeometry(10, 180, 480, 210)
        self.consol.repaint()
        # Labels
        self.IDLabel.hide()
        self.DLCLabel.hide()
        self.DataLabel.hide()
        self.label_avto.setText(Var.LABEL_CURRENT_AUTO + 'Arkana')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.label_avto.repaint()
        self.current_auto = "Arkana"

    def can_logs_player(self):
        if Var.thread_flag == "on":
            self.stop_thread_info()
            Var.thread_flag = "off"  # Thread OFF
        self.label_avto.setText('Выберите log-файл...')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.label_combo_box.setText('Выберите скорость CAN: ')
        self.label_combo_box.resize(self.label_combo_box.sizeHint())
        self.combo_box.clear()
        self.combo_box.addItem("250000 кбит/с")
        self.combo_box.addItem("500000 кбит/с")
        self.combo_box.addItem("1000000 кбит/с")
        self.current_auto = "Logs Player"

#Actions

    def showDialog(self):  # "Open File" Dialog, select file for open
        home_dir = str(Path.home())
        # self.fileName = QFileDialog.getOpenFileName(self, 'Open file', home_dir)
        self.fileName = QFileDialog.getSaveFileName(self, 'Open file', home_dir)

        # Add command to Info (TextBrowser)
        self.consol.append("-----------------------")
        self.consol.append('Log-файл: ' + str(self.fileName))
        self.consol.append("-----------------------")
        self.consol.repaint()
        print(str(self.fileName))

        # if fname[0]:
        #     f = open(fname[0], 'r')
        #
        #     with f:
        #         data = f.read()
        #         self.consol.setText(data)
        #         # self.textEdit.setText(data)

    def onActivated_box(self, text):
        # self.consol.setText(text)  # Replace old text to new one
        self.consol.append(text)    # Add new text to old
        self.consol.repaint()

    def onActivated_dlc_Combo_Box(self):
        if self.dlcCombo.currentText() == "1":
            self.dataInput8.hide()
            self.dataInput7.hide()
            self.dataInput6.hide()
            self.dataInput5.hide()
            self.dataInput4.hide()
            self.dataInput3.hide()
            self.dataInput2.hide()
            self.DataLabel.move(125, 135)
        if self.dlcCombo.currentText() == "2":
            self.dataInput8.hide()
            self.dataInput7.hide()
            self.dataInput6.hide()
            self.dataInput5.hide()
            self.dataInput4.hide()
            self.dataInput3.hide()
            self.dataInput2.show()
            self.DataLabel.move(148, 135)
        if self.dlcCombo.currentText() == "3":
            self.dataInput8.hide()
            self.dataInput7.hide()
            self.dataInput6.hide()
            self.dataInput5.hide()
            self.dataInput4.hide()
            self.dataInput3.show()
            self.dataInput2.show()
            self.DataLabel.move(166, 135)
        if self.dlcCombo.currentText() == "4":
            self.dataInput8.hide()
            self.dataInput7.hide()
            self.dataInput6.hide()
            self.dataInput5.hide()
            self.dataInput4.show()
            self.dataInput3.show()
            self.dataInput2.show()
            self.DataLabel.move(188, 135)
        if self.dlcCombo.currentText() == "5":
            self.dataInput8.hide()
            self.dataInput7.hide()
            self.dataInput6.hide()
            self.dataInput5.show()
            self.dataInput4.show()
            self.dataInput3.show()
            self.dataInput2.show()
            self.DataLabel.move(208, 135)
        if self.dlcCombo.currentText() == "6":
            self.dataInput8.hide()
            self.dataInput7.hide()
            self.dataInput6.show()
            self.dataInput5.show()
            self.dataInput4.show()
            self.dataInput3.show()
            self.dataInput2.show()
            self.DataLabel.move(230, 135)
        if self.dlcCombo.currentText() == "7":
            self.dataInput8.hide()
            self.dataInput7.show()
            self.dataInput6.show()
            self.dataInput5.show()
            self.dataInput4.show()
            self.dataInput3.show()
            self.dataInput2.show()
            self.DataLabel.move(245, 135)
        if self.dlcCombo.currentText() == "8":
            self.dataInput8.show()
            self.dataInput7.show()
            self.dataInput6.show()
            self.dataInput5.show()
            self.dataInput4.show()
            self.dataInput3.show()
            self.dataInput2.show()
            self.DataLabel.move(270, 135)


    def onActivated_Combo_Box(self):
        if self.current_auto == "Vesta":
                if self.combo_box.currentText() == "Open Hood/Trunk/Doors":
                    # Show CheckBoxs
                    self.open_drivers_door_CheckBox.show()
                    self.open_trunk_CheckBox.show()
                    self.open_back_doors_CheckBox.show()
                    self.open_hood_CheckBox.show()
                    self.open_passenger_door_CheckBox.show()
                    # Text Browser Geomerty
                    self.consol.setGeometry(10, 240, 480, 150)
                    self.consol.repaint()
                    # Buttons
                    self.send_button.move(20, 200)
                    self.clear_button.move(280, 200)
                else:
                    # Hide CheckBoxs
                    self.open_drivers_door_CheckBox.hide()
                    self.open_trunk_CheckBox.hide()
                    self.open_back_doors_CheckBox.hide()
                    self.open_hood_CheckBox.hide()
                    self.open_passenger_door_CheckBox.hide()
                    # Send Button
                    self.send_button.move(20, 135)
                    # Clear Button
                    self.clear_button.move(280, 135)
                    # Text Browser Geomerty
                    self.consol.setGeometry(10, 180, 480, 210)
                    self.consol.repaint()


    def click_send_button(self):
        try:
            command = self.combo_box.currentText()  # Current item from combo box
            Var.command = self.combo_box.currentText()
            Var.command_1 = self.combo_box_1.currentText()
            Var.command_2 = self.combo_box_2.currentText()
            Var.command_3 = self.combo_box_3.currentText()
            current_auto = self.current_auto
            if (Var.command == Var.command_1 and (Var.command != '' and Var.command_1 != ''))\
                    or (Var.command_1 == Var.command_2 and (Var.command_1 != '' and Var.command_2 != '')) \
                    or (Var.command_2 == Var.command_3 and (Var.command_2 != '' and Var.command_3 != '')) \
                    or (Var.command == Var.command_3 and (Var.command != '' and Var.command_3 != '')):
                self.infoLable.setText("""
            <head>
            <style>
            * {
            color: red;
            text-align: right;
            }
            </head>
            </style>
            <body>
            <div>The same commands are selected</div>
            </body>
            """)

            # Check COM-port Status
            curent_channel = auto_port_scan()
            if curent_channel == "":
                self.com_port_not_found_info()
            else:
                # Send commands
                if current_auto == "Custom command":
                    Var.id = int(self.canIdInput.text(), 16)
                    if self.canIdInput.text() == '':
                        self.infoLable.setText("""
                        <head>
                        <style>
                        * {
                        color: red;
                        text-align: right;
                        }
                        </head>
                        </style>
                        <body>
                        <div>Incorrect CAN ID</div>
                        </body>
                        """)
                        self.consol.append(('Incorrect CAN ID'))
                    elif int(self.dlcCombo.currentText()) == 6 and self.dataInput1.text() == ''\
                            or self.dataInput2.text() == '' or self.dataInput3.text() == ''\
                            or self.dataInput4.text() == '' or self.dataInput5.text() == '' \
                            or self.dataInput6.text() == '':
                        self.infoLable.setText("""
                                            <head>
                                            <style>
                                            * {
                                            color: red;
                                            text-align: right;
                                            }
                                            </head>
                                            </style>
                                            <body>
                                            <div>Incorrect DATA</div>
                                            </body>
                                            """)
                        self.consol.append(('Incorrect  DATA'))
                        if int(self.dlcCombo.currentText()) == 7 and self.dataInput7.text() == '':
                            self.infoLable.setText("""
                                                <head>
                                                <style>
                                                * {
                                                color: red;
                                                text-align: right;
                                                }
                                                </head>
                                                </style>
                                                <body>
                                                <div>Incorrect DATA</div>
                                                </body>
                                                """)
                            self.consol.append(('Incorrect  DATA'))
                            if int(self.dlcCombo.currentText()) == 8 and self.dataInput8.text() == '':
                                self.infoLable.setText("""
                                                    <head>
                                                    <style>
                                                    * {
                                                    color: red;
                                                    text-align: right;
                                                    }
                                                    </head>
                                                    </style>
                                                    <body>
                                                    <div>Incorrect DATA</div>
                                                    </body>
                                                    """)
                                self.consol.append(('Incorrect  DATA'))
                    else:
                        Var.dlc = int(self.dlcCombo.currentText())
                        if Var.dlc == 8:
                            Var.data = [int(self.dataInput1.text(), 16),
                                        int(self.dataInput2.text(), 16),
                                        int(self.dataInput3.text(), 16),
                                        int(self.dataInput4.text(), 16),
                                        int(self.dataInput5.text(), 16),
                                        int(self.dataInput6.text(), 16),
                                        int(self.dataInput7.text(), 16),
                                        int(self.dataInput8.text(), 16)
                                        ]
                        elif Var.dlc == 7:
                            Var.data = [int(self.dataInput1.text(), 16),
                                        int(self.dataInput2.text(), 16),
                                        int(self.dataInput3.text(), 16),
                                        int(self.dataInput4.text(), 16),
                                        int(self.dataInput5.text(), 16),
                                        int(self.dataInput6.text(), 16),
                                        int(self.dataInput7.text(), 16)
                                        ]
                        elif Var.dlc == 6:
                            Var.data = [int(self.dataInput1.text(), 16),
                                        int(self.dataInput2.text(), 16),
                                        int(self.dataInput3.text(), 16),
                                        int(self.dataInput4.text(), 16),
                                        int(self.dataInput5.text(), 16),
                                        int(self.dataInput6.text(), 16)
                                        ]
                        elif Var.dlc == 5:
                            Var.data = [int(self.dataInput1.text(), 16),
                                        int(self.dataInput2.text(), 16),
                                        int(self.dataInput3.text(), 16),
                                        int(self.dataInput4.text(), 16),
                                        int(self.dataInput5.text(), 16)
                                        ]
                        elif Var.dlc == 4:
                            Var.data = [int(self.dataInput1.text(), 16),
                                        int(self.dataInput2.text(), 16),
                                        int(self.dataInput3.text(), 16),
                                        int(self.dataInput4.text(), 16)
                                        ]
                        elif Var.dlc == 3:
                            Var.data = [int(self.dataInput1.text(), 16),
                                        int(self.dataInput2.text(), 16),
                                        int(self.dataInput3.text(), 16)
                                        ]
                        elif Var.dlc == 2:
                            Var.data = [int(self.dataInput1.text(), 16),
                                        int(self.dataInput2.text(), 16)
                                        ]
                        elif Var.dlc == 1:
                            Var.data = [int(self.dataInput1.text(), 16)
                                        ]
                        Var.custom_command_send_flag = "on"
                        # self.send_command_info()
                if current_auto == "Polo":
                    if command == "Volume Up":
                        Var.polo_volume_up_flag = "on"
                        # self.send_command_info()
                    if command == "Volume Down":
                        Var.polo_volume_down_flag = "on"
                        # self.send_command_info()
                    if command == "Next Track":
                        Var.polo_next_track_flag = "on"
                        # self.send_command_info()
                    if command == "Previous Track":
                        Var.polo_pre_track_flag = "on"
                        # self.send_command_info()
                    if command == "Phone":
                        Var.polo_phone_flag = "on"
                        # self.send_command_info()
                    if command == "Voice":
                        Var.polo_voice_flag = "on"
                        # self.send_command_info()
                    if command == "Mute":
                        Var.polo_mute_flag = "on"
                        # self.send_command_info()
                    if command == "Reversing On":
                        Var.polo_reversing_on_flag = "on"
                        # self.send_command_info()
                    if command == "Reversing Off":
                        Var.polo_reversing_off_flag = "on"
                        # self.send_command_info()
                    if command == "Lighting on":
                        Var.polo_lighting_on_flag = "on"
                        # self.send_command_info()
                    if command == "Lighting off":
                        Var.polo_lighting_off_flag = "on"
                        # self.send_command_info()

                if current_auto == "Octavia":
                    if command == "Volume Up":
                        Var.octavia_volume_up_flag = "on"
                        # self.send_command_info()
                    if command == "Volume Down":
                        Var.octavia_volume_down_flag = "on"
                        # self.send_command_info()
                    if command == "Next Track":
                        Var.octavia_next_track_flag = "on"
                        # self.send_command_info()
                    if command == "Previous Track":
                        Var.octavia_pre_track_flag = "on"
                        # self.send_command_info()
                    if command == "Next Track long press":
                        Var.octavia_next_long_press_flag = "on"
                        # self.send_command_info()
                    if command == "Previous Track long press":
                        Var.octavia_pre_long_press_flag = "on"
                        # self.send_command_info()
                    if command == "Phone":
                        Var.octavia_phone_flag = "on"
                        # self.send_command_info()
                    if command == "Voice":
                        Var.octavia_voice_flag = "on"
                        # self.send_command_info()
                    if command == "Mute":
                        Var.octavia_mute_flag = "on"
                        # self.send_command_info()

                if current_auto == "Nissan":
                    if command == "Volume Up":
                        Var.nissan_volume_up_flag = "on"
                        # self.send_command_info()
                    if command == "Volume Down":
                        Var.nissan_volume_down_flag = "on"
                        # self.send_command_info()
                    if command == "Next Track":
                        Var.nissan_next_track_flag = "on"
                        # self.send_command_info()
                    if command == "Previous Track":
                        Var.nissan_pre_tack_flag = "on"
                        # self.send_command_info()
                    if command == "Phone/Voice Long Press":
                        Var.nissan_phone_voice_long_press_flag = "on"
                        # self.send_command_info()
                    if command == "Phone/Voice Short Press":
                        Var.nissan_phone_voice_short_press_flag = "on"
                        # self.send_command_info()
                    if command == "Hang Up":
                        Var.nissan_hang_up_flag = "on"
                        # self.send_command_info()
                    if command == "Mode":
                        Var.nissan_mode_flag = "on"
                        # self.send_command_info()

                if current_auto == "Mitsubishi":
                    # Air Conditioning
                    if command == "AC On":
                        Var.mitsubishi_auto_on_flag = "on"
                        # self.send_command_info()
                    if command == "AC Off":
                        Var.mitsubishi_auto_off_flag = "on"
                        # self.send_command_info()
                    if command == "Activation Dual fun mode":
                        Var.mitsubishi_dual_flag = "on"
                        # self.send_command_info()
                    if command == "Fun Auto mode on":
                        Var.mitsubishi_auto_on_flag = "on"
                        # self.send_command_info()
                    if command == "Fun Auto mode off":
                        Var.mitsubishi_auto_off_flag = "on"
                        # self.send_command_info()
                    if command == "Left Tem. regulation up":
                        Var.mitsubishi_left_tem_regulation_flag = "on"
                        # self.send_command_info()
                    if command == "Right Tem. regulation up":
                        Var.mitsubishi_right_tem_regulation_flag = "on"
                        # self.send_command_info()
                    if command == "Ventilator Power up":
                        Var.mitsubishi_ventilator_power_flag = "on"
                        # self.send_command_info()
                    # Fan Direction
                    if command == "Face":
                        Var.mitsubishi_face_flag = "on"
                        # self.send_command_info()
                    if command == "Face & Foot":
                        Var.mitsubishi_face_foot_flag = "on"
                        # self.send_command_info()
                    if command == "Foot":
                        Var.mitsubishi_foot_flag = "on"
                        # self.send_command_info()
                    if command == "Foot & Windscreen":
                        Var.mitsubishi_foot_windscreen_flag = "on"
                        # self.send_command_info()
                    # Rear Window Heating
                    if command == "Rear Window Heating On":
                        Var.mitsubishi_rear_window_heating_on_flag = "on"
                        # self.send_command_info()
                    if command == "Rear Window Heating Off":
                        Var.mitsubishi_rear_window_heating_off_flag = "on"
                        # self.send_command_info()
                    # Recirculation
                    if command == "Recirculation Cabin":
                        Var.mitsubishi_recirculation_cabin_flag = "on"
                        # self.send_command_info()
                    if command == "Recirculation Outside":
                        Var.mitsubishi_recirculation_outside_flag = "on"
                        # self.send_command_info()
                    # Reversing On/Off
                    if command == "Reversing On":
                        Var.mitsubishi_reversing_on_flag = "on"
                        # self.send_command_info()
                    if command == "Reversing Off":
                        Var.mitsubishi_reversing_off_flag = "on"
                        # self.send_command_info()
                    # Windscreen Heating
                    if command == "Windscreen Heating On":
                        Var.mitsubishi_rear_window_heating_on_flag = "on"
                        # self.send_command_info()
                    if command == "Windscreen Heating Off":
                        Var.mitsubishi_rear_window_heating_off_flag = "on"
                        # self.send_command_info()

                if current_auto == "Vesta":
                    # if self.combo_box_3.isVisible():
                    #     #print('combo 3 is visible')
                    #     Var.vesta_4_commands_flag = "on"
                    #     # self.send_command_info()
                    # elif self.combo_box_2.isVisible():
                    #     #print('combo 2 is visible')
                    #     Var.vesta_3_commands_flag = "on"
                    #     # self.send_command_info()
                    # elif self.combo_box_1.isVisible():
                    #     #print('combo 1 is visible')
                    #     Var.vesta_2_commands_flag = "on"
                    #     # self.send_command_info()
                    # else:
                        #print('no one combo is visible')
                        if command == "Handbrake (Unblock AA)":
                            Var.vesta_Handbrake_command_flag = "on"
                            # self.send_command_info()
                        elif command == "Speed_5_km_h (Unblock Browser)":
                            Var.vesta_speed_5_km_h_comand_flag = "on"
                            # self.send_command_info()
                        elif command == "Speed_15_km_h (Block Browser)":
                            Var.vesta_speed_15_km_h_command_flag = "on"
                            # self.send_command_info()
                        elif command == "Reversing On (Camera On)":
                            Var.vesta_reversing_on_command_flag = "on"
                            # self.send_command_info()
                        elif command == "Reversing Off (Camera Off)":
                            Var.vesta_reversing_off_command_flag = "on"
                            # self.send_command_info()
                        elif command == "Parktronic zero position":
                            Var.vesta_parktronic_zero_position_command_flag = "on"
                            # self.send_command_info()
                        elif command == "Parktronic leftmost position":
                            Var.vesta_parktronic_leftmost_position_command_flag = "on"
                            # self.send_command_info()
                        elif command == "Parktronic rightmost position":
                            Var.vesta_parktronic_rightmost_position_command_flag = "on"
                            # self.send_command_info()
                        elif command == "External Sensor Temperature -18":
                            Var.vesta_external_sensor_temperature_minus_18 = "on"
                            # self.send_command_info()
                        elif command == "External Sensor Temperature 0":
                            Var.vesta_external_sensor_temperature_0 = "on"
                            # self.send_command_info()
                        elif command == "External Sensor Temperature +12":
                            Var.vesta_external_sensor_temperature_plus_12 = "on"
                            # self.send_command_info()
                        elif command == "Open Hood/Trunk/Doors":            #CheckBoxs items
                            Var.vesta_5_commands_flag = "on"
                            if self.open_hood_CheckBox.isChecked() == True:
                                Var.vesta_open_hood = "on"
                            else:
                                Var.vesta_open_hood = "off"
                            if self.open_trunk_CheckBox.isChecked() == True:
                                Var.vesta_open_trunk = "on"
                            else:
                                Var.vesta_open_trunk = "off"
                            if self.open_drivers_door_CheckBox.isChecked() == True:
                                Var.vesta_open_drivers_door = "on"
                            else:
                                Var.vesta_open_drivers_door = "off"
                            if self.open_passenger_door_CheckBox.isChecked() == True:
                                Var.vesta_open_passengers_door = "on"
                            else:
                                Var.vesta_open_passengers_door = "off"
                            if self.open_back_doors_CheckBox.isChecked() == True:
                                Var.vesta_open_back_door = "on"
                            else:
                                Var.vesta_open_back_door = "off"
                            # self.send_command_info()

                if current_auto == "Granta":
                    if command == "Handbrake (Unblock AA)":
                        Var.granta_handbrake_flag = "on"
                        # self.send_command_info()
                    if command == "Speed_5_km_h (Unblock Browser)":
                        Var.granta_speed_5_km_h_flag = "on"
                        # self.send_command_info()
                    if command == "Speed_15_km_h (Block Browser)":
                        Var.granta_speed_15_km_h_flag = "on"
                        # self.send_command_info()
                    if command == "External Sensor Temperature -18":
                        Var.granta_external_sensor_temperature_minus_18 = "on"
                        # self.send_command_info()
                    if command == "External Sensor Temperature 0":
                        Var.granta_external_sensor_temperature_0 = "on"
                        # self.send_command_info()
                    if command == "External Sensor Temperature +12":
                        Var.granta_external_sensor_temperature_plus_12 = "on"
                        # self.send_command_info()

                if current_auto == "Arkana":
                    if command == "Voice":
                        Var.arkana_voice_command_flag = "on"
                        # self.send_command_info()
                    if command == "Handbrake (Unblock AA)":
                        Var.arkana_Handbrake_command_flag = "on"
                        # self.send_command_info()

                #!!!Следует вынести в отдельный файл!!!
                #if current_auto == "Logs Player":
                #    # Bitrate
                #    if command == "250000 кбит/с":
                #        bitrate = 250000
                #    if command == "500000 кбит/с":
                #        bitrate = 500000
                #    if command == "1000000 кбит/с":
                #        bitrate = 1000000
                #    bus = Bus(bustype=bus_type, channel=curent_channel, bitrate=bitrate)
                #    can_parser.parser_for_can_hacker_files.parser(self.fileName, bus)

                # Add command to Info (TextBrowser)
                if current_auto == "":
                    self.consol.append("")
                    self.consol.repaint()
                # Add command to Info (TextBrowser)
                elif current_auto == "Custom command":
                    self.consol.append("-----------------------")
                    self.consol.append("Команда:")
                    self.consol.append("ID: [" + str(Var.id) + "] DLC: [" + str(Var.dlc) + "] Data: " + str(Var.data) + " - отправлена")
                    self.consol.append("Date: " + time.ctime())
                    self.consol.append("-----------------------")
                    self.consol.repaint()
                # Add command to Info (TextBrowser)
                elif command == "Open Hood/Trunk/Doors":
                    self.consol.append("-----------------------")
                    # print("vesta_open_trunk", vesta_open_trunk)
                    # print("vesta_open_hood", vesta_open_hood)
                    # print("vesta_open_back_door", vesta_open_back_door)
                    # print("vesta_open_drivers_door", vesta_open_drivers_door)
                    # print("vesta_open_passengers_door", vesta_open_passengers_door)
                    self.consol.append("Команда:")
                    self.consol.append("Open Hood/Trunk/Doors - отправлена")
                    self.consol.append(" - Hood: " + Var.vesta_open_hood)
                    self.consol.append(" - Trunk: " + Var.vesta_open_trunk)
                    self.consol.append(" - Driver Door: " + Var.vesta_open_drivers_door)
                    self.consol.append(" - Passenger Door: " + Var.vesta_open_passengers_door)
                    self.consol.append(" - Back Doors: " + Var.vesta_open_back_door)
                    self.consol.append("Date: " + time.ctime())
                    self.consol.append("-----------------------")
                    self.consol.repaint()
                else:
                    self.consol.append("-----------------------")
                    self.consol.append("Авто: " + current_auto)
                    self.consol.append("Команда: " + command + " - отправлена")
                    self.consol.append("Date: " + time.ctime())
                    self.consol.append("-----------------------")
                    self.consol.repaint()
                    pass
        except ValueError:
            if current_auto == "Custom command":
                self.consol.append("")
                self.consol.append("НЕВЕРНЫЙ ВВОД ДАННЫХ!")
                self.consol.append("")
                print('Неверный ввод данных!')
        except Exception:
            self.consol.append("")
            self.consol.append("EXCEPTION!")
            self.consol.append("")
            print('Exception')
        finally:
            pass

    def click_clear_button(self):
        self.consol.clear()
        self.consol.repaint()

    def click_start_thread_button(self):
        self.start_thread_button.setEnabled(False)
        self.start_thread_button.repaint()
        self.stop_thread_button.setEnabled(True)
        self.stop_thread_button.repaint()

        self.combo_box.setEnabled(True)
        self.combo_box.repaint()
        self.send_button.setEnabled(True)
        self.send_button.repaint()
        self.clear_button.setEnabled(True)
        self.clear_button.repaint()
        channel = auto_port_scan()
        if channel == "":
            self.com_port_not_found_info()
            self.start_thread_button.setEnabled(True)
            self.start_thread_button.repaint()
            self.stop_thread_button.setEnabled(False)
            self.stop_thread_button.repaint()
            self.combo_box.setEnabled(False)
            self.combo_box.repaint()
            self.combo_box_1.setEnabled(False)
            self.combo_box_1.repaint()
            self.combo_box_2.setEnabled(False)
            self.combo_box_2.repaint()
            self.combo_box_3.setEnabled(False)
            self.combo_box_3.repaint()
            self.addComboBoxButton.setEnabled(False)
            self.addComboBoxButton.repaint()
            self.hideComboBoxButton.setEnabled(False)
            self.hideComboBoxButton.repaint()
            self.send_button.setEnabled(False)
            self.send_button.repaint()
        else:
            if self.current_auto == "Custom command":
                self.bitrateComboBox.setEnabled(False)
                self.canIdInput.setEnabled(True)
                self.dlcCombo.setEnabled(True)
                self.dataInput1.setEnabled(True)
                self.dataInput2.setEnabled(True)
                self.dataInput3.setEnabled(True)
                self.dataInput4.setEnabled(True)
                self.dataInput5.setEnabled(True)
                self.dataInput6.setEnabled(True)
                self.dataInput7.setEnabled(True)
                self.dataInput8.setEnabled(True)
                # Bitrate for Custom Tread
                if self.bitrateComboBox.currentText() == "250000 (bit/s)":
                    Var.bitrate = 250000
                if self.bitrateComboBox.currentText() == "500000 (bit/s)":
                    Var.bitrate = 500000
                if self.bitrateComboBox.currentText() == "1000000 (bit/s)":
                    Var.bitrate = 1000000
                Var.id = int(self.canIdInput.text(), 16)
                Var.thread_flag = "on"
                Var.custom_command_flag = "on"
                thread_custom = ThreadCustom("Custom_thread")
                thread_custom.start()
                self.start_thread_info()
            elif self.current_auto == "Polo":
                Var.thread_flag = "on"
                Var.polo_flag = "on"
                thread_polo = ThreadPolo("Polo_thread")
                thread_polo.start()
                self.start_thread_info()
            elif self.current_auto == "Octavia":
                Var.thread_flag = "on"
                Var.octavia_flag = "on"
                thread_octavia = ThreadOctavia("Octavia_thread")
                thread_octavia.start()
                self.start_thread_info()
            elif self.current_auto == "Nissan":
                Var.thread_flag = "on"
                Var.nissan_flag = "on"
                thread_nissan = ThreadNissan("Nissan_thread")
                thread_nissan.start()
                self.start_thread_info()
            elif self.current_auto == "Mitsubishi":
                Var.thread_flag = "on"
                Var.mitsubishi_flag = "on"
                thread_mitsubishi = ThreadMitsubishi("Mitsubishi_thread")
                thread_mitsubishi.start()
                self.start_thread_info()
            elif self.current_auto == 'Vesta':
                Var.thread_flag = "on"
                Var.vesta_flag = "on"
                thread_vesta = ThreadVesta("Vesta_thread")
                thread_vesta.start()
                self.start_thread_info()
                self.combo_box_1.setEnabled(True)
                self.combo_box_1.repaint()
                self.combo_box_2.setEnabled(True)
                self.combo_box_2.repaint()
                self.combo_box_3.setEnabled(True)
                self.combo_box_3.repaint()
                self.addComboBoxButton.setEnabled(True)
                self.addComboBoxButton.repaint()
                self.hideComboBoxButton.setEnabled(True)
                self.hideComboBoxButton.repaint()
            elif self.current_auto == "Granta":
                Var.thread_flag = "on"
                Var.granta_flag = "on"
                thread_granta = ThreadGranta("Granta_thread")
                thread_granta.start()
                self.start_thread_info()
            elif self.current_auto == 'Arkana':
                Var.thread_flag = "on"
                Var.arkana_flag = "on"
                thread_arkana = ThreadArkana("Arkana_thread")
                thread_arkana.start()
                self.start_thread_info()

    def click_stop_thread_button(self):
        Var.thread_flag = "off" #Thread Off
        self.stop_thread_info()
        self.bitrateComboBox.setEnabled(True)
        self.stop_thread_button.setEnabled(False)
        self.stop_thread_button.repaint()
        self.start_thread_button.setEnabled(True)
        self.start_thread_button.repaint()
        self.combo_box.setEnabled(False)
        self.combo_box.repaint()
        self.combo_box_1.setEnabled(False)
        self.combo_box_1.repaint()
        self.combo_box_2.setEnabled(False)
        self.combo_box_2.repaint()
        self.combo_box_3.setEnabled(False)
        self.combo_box_3.repaint()
        self.addComboBoxButton.setEnabled(False)
        self.addComboBoxButton.repaint()
        self.hideComboBoxButton.setEnabled(False)
        self.hideComboBoxButton.repaint()
        self.send_button.setEnabled(False)
        self.send_button.repaint()
        self.clear_button.setEnabled(False)
        self.clear_button.repaint()
        self.canIdInput.setEnabled(False)
        self.dlcCombo.setEnabled(False)
        self.dataInput1.setEnabled(False)
        self.dataInput2.setEnabled(False)
        self.dataInput3.setEnabled(False)
        self.dataInput4.setEnabled(False)
        self.dataInput5.setEnabled(False)
        self.dataInput6.setEnabled(False)
        self.dataInput7.setEnabled(False)
        self.dataInput8.setEnabled(False)

    def closeEvent(self, event):
        if self.myclose:
            if Var.thread_flag == "on":
                self.stop_thread_info()
                Var.thread_flag = "off"  # Thread OFF
            else:
                print("Application closed")

    def switchTheme(self, pressed):
        self.col = QColor(0, 0, 0)
        source = self.sender()
        if pressed:
            self.col.setBlue(255)
            self.themeButtonSwitch.setText('Red')
            #self.themeButtonSwitch.setStyleSheet(Var.DARK_THEME)
            #pallete = QPalette()
            #pallete.setColor(QPalette.Window, QColor(105, 105, 105))
            #pallete.setColor(QPalette.Button, QColor(105, 105, 105))
            #self.setPalette(pallete)
            #self.setStyleSheet(Var.DARK_THEME)
            #self.clear_button.setGeometry(280, 130, 200, 22)
        else:
            self.col.setRed(255)
            self.themeButtonSwitch.setText('Blue')
            #pallete = QPalette()
            #pallete.setColor(QPalette.Window, QColor(255, 255, 255))
            #self.setPalette(pallete)
        self.box.setStyleSheet("QFrame { background-color: %s }" % self.col.name())
        self.consol.append(f"{'12:00'}: switch color to {self.col.name()}")

    def hideConsol(self):
        if self.consol.isHidden() == True:
            self.consol.show()
            self.hideConsolButton.setText('-')
            self.setMaximumSize(500, 410)
            self.resize(500, 410)
            self.setFixedSize(500, 410)
            self.repaint()
            self.clear_button.setEnabled(True)
        else:
            self.consol.hide()
            self.hideConsolButton.setText('+')
            self.statusBar().repaint()
            self.setMaximumSize(500, 260)
            self.resize(500, 260)
            self.setFixedSize(500, 260)
            self.repaint()
            self.clear_button.setEnabled(False)

    def setDataFromDlc(self):
        n = self.dlcCombo.currentText()
        if n == '6':
            self.dataInput7.hide()
            self.dataInput7.clear()
            self.dataInput8.hide()
            self.dataInput8.clear()
        elif n == '7':
            self.dataInput7.show()
            self.dataInput8.hide()
            self.dataInput8.clear()
        elif n == '8':
            self.dataInput7.show()
            self.dataInput8.show()

# Information messages:

    def stop_thread_info(self):
        # Add command to Info (TextBrowser)
        print("Pre thread OFF")
        self.consol.append("Поток с командами остановлен")
        self.infoLable.setText("""
        <head>
        <style>
        * {
        text-align: right;
        }
        </head>
        </style>
        <body>
        <div>Thread is stopped</div>
        </body>
        """)
        self.consol.repaint()

    def com_port_not_found_info(self):
        # Add Info (TextBrowser)
        self.consol.append("COM port not found")
        self.infoLable.setText("""
        <head>
        <style>
        * {
        text-align: right;
        }
        </head>
        </style>
        <body>
        <div>Com port not found</div>
        </body>
        """)
        self.consol.repaint()

    def start_thread_info(self):
        # Add test to Info (TextBrowser)
        self.consol.append(f"Запущен поток с командами для {self.current_auto}")
        self.infoLable.setText("""
        <head>
        <style>
        * {
        text-align: right;
        }
        </head>
        </style>
        <body>
        <div>Thread is started</div>
        </body>
        """)
        self.consol.repaint()

    # def send_command_info(self):
    #     self.infoLable.setText("""
    #             <head>
    #             <style>
    #             * {
    #             text-align: right;
    #             }
    #             </head>
    #             </style>
    #             <body>
    #             <div>Command is send</div>
    #             <div></div>
    #             </body>
    #             """)
    #     self.consol.append("Command is send")
