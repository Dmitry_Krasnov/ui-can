import time
import can

# -------------------------------------
#           variables
# -------------------------------------
oct_rapid_msg = can.Message(arbitration_id=0x315,
                            data=[0, 64, 0, 0, 0, 0, 0, 0],
                            is_extended_id=False)


# ---------------------------------------------
# List of commands for Octavia & Rapid
# ---------------------------------------------
class Octavia_Rapid:

    @staticmethod
    def can_bitrate():  # Octavia_Rapid CAN Bitrate
        return 500000

    @staticmethod
    def volume_up(bus):  # VoL+
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x12, 0x00, 0x01, 0x50]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x50]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def volume_down(bus):  # VoL-
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x12, 0x00, 0x0F, 0x50]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x50]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def voice(bus):  # VOICE
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x19, 0x00, 0x01, 0x50]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x50]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def mute(bus):  # MUTE
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x13, 0x00, 0x01, 0x50]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x50]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def phone(bus):  # to take a call
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x1C, 0x00, 0x01, 0x50]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x50]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def next_track(bus):  # NEXT TRACK
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x15, 0x00, 0x01, 0x23]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x23]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def next_long_press(bus):  # NEXT LONG PRESS
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x15, 0x00, 0x01, 0x23]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x15, 0x00, 0x04, 0x23]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x23]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def pre_track(bus):  # PREVIOUS TRACK
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x16, 0x00, 0x01, 0x23]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x23]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def pre_long_press(bus):  # PREVIOUS LONG PRESS
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x16, 0x00, 0x01, 0x23]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x16, 0x00, 0x04, 0x23]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x23]
        bus.send(oct_rapid_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
