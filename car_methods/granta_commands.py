import time
import can

# -------------------------------------
#           variables
# -------------------------------------
granta_msg = can.Message(arbitration_id=0x315,
                       data=[0, 64, 0, 0, 0, 0, 0, 0],
                       is_extended_id=False)


# ---------------------------------------------
# List of commands for Vesta
# ---------------------------------------------
class Granta:

    @staticmethod
    def handbrake(bus):  # Handbrake (start AA)
        can.Message.arbitration_id = 0x2DE
        can.Message.dlc = 8
        can.Message.data = [0x00, 0x00, 0x01, 0x08, 0x00, 0x00, 0xB0, 0x00]
        bus.send(granta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x01, 0x08, 0x00, 0x00, 0xB0, 0x00]
        bus.send(granta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def Speed_5_km_h(bus):  # Speed about 5 km/h
        can.Message.arbitration_id = 0x27C
        can.Message.dlc = 8
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x0B]
        bus.send(granta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x00, 0x0B]
        bus.send(granta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def Speed_15_km_h(bus):  # Speed about 15 km/h
        can.Message.arbitration_id = 0x27C
        can.Message.dlc = 8
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x02, 0x74, 0x00, 0x08]
        bus.send(granta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x02, 0x74, 0x00, 0x08]
        bus.send(granta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def external_sensor_temperature_minus_18(bus):
        can.Message.arbitration_id = 0x280
        can.Message.dlc = 8
        can.Message.data = [0x31, 0x34, 0x16, 0x10, 0x00, 0x1E, 0x00, 0x00]
        bus.send(granta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x31, 0x34, 0x16, 0x10, 0x00, 0x1E, 0x00, 0x00]
        bus.send(granta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def external_sensor_temperature_0(bus):
        can.Message.arbitration_id = 0x280
        can.Message.dlc = 8
        can.Message.data = [0x31, 0x34, 0x28, 0x10, 0x00, 0x1E, 0x00, 0x00]
        bus.send(granta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x31, 0x34, 0x28, 0x10, 0x00, 0x1E, 0x00, 0x00]
        bus.send(granta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def external_sensor_temperature_plus_12(bus):
        can.Message.arbitration_id = 0x280
        can.Message.dlc = 8
        can.Message.data = [0x31, 0x34, 0x34, 0x10, 0x00, 0x1E, 0x00, 0x00]
        bus.send(granta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x31, 0x34, 0x34, 0x10, 0x00, 0x1E, 0x00, 0x00]
        bus.send(granta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
