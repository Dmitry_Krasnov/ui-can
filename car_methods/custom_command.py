import time
import can

class CustomCommand():
    def __init__(self, id, dlc, data):
        self.id = id
        self.dlc = dlc
        self.data = data

    def sendCommand(self, bus):
        msg = can.Message(arbitration_id=0x315,
                                data=[0, 64, 0, 0, 0, 0, 0, 0],
                                is_extended_id=False)
        can.Message.arbitration_id = self.id
        can.Message.dlc = self.dlc
        can.Message.data = self.data
        #print(can.Message.arbitration_id, can.Message.dlc, can.Message.data) #Если необходиом проверить данные команды
        bus.send(msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = self.data
        bus.send(msg)  # Connect and send frame by CANable
        time.sleep(0.1)