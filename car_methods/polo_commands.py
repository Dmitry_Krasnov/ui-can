import time
import can

# -------------------------------------
#           variables
# -------------------------------------
polo_msg = can.Message(arbitration_id=0x315,
                       data=[0, 64, 0, 0, 0, 0, 0, 0],
                       is_extended_id=False)


# ---------------------------------------------
# List of commands for Polo
# ---------------------------------------------
class Polo:

    @staticmethod
    def can_bitrate():  # Polo CAN Bitrate
        return 500000

    @staticmethod
    def wake_up(bus):  # wake_up
        can.Message.arbitration_id = 0x621
        can.Message.dlc = 8
        can.Message.data = [0x0F, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.01)
        can.Message.data = [0x0F, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.01)

    @staticmethod
    def volume_up(bus):  # Vol+
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x06, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.arbitration_id = 0x5BF
        can.Message.data = [0x00, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable

    @staticmethod
    def volume_down(bus):  # VoL-
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x07, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.arbitration_id = 0x5BF
        can.Message.data = [0x00, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def voice(bus):  # VOICE
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x2A, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.arbitration_id = 0x5BF
        can.Message.data = [0x00, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def mute(bus):  # MUTE
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x2B, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.arbitration_id = 0x5BF
        can.Message.data = [0x00, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def phone(bus):  # to take a call
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x1A, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.arbitration_id = 0x5BF
        can.Message.data = [0x00, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def next_track(bus):  # NEXT TRACK
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x02, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.arbitration_id = 0x5BF
        can.Message.data = [0x00, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def pre_track(bus):  # PREVIOUS TRACK
        can.Message.arbitration_id = 0x5BF
        can.Message.dlc = 4
        can.Message.data = [0x03, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.arbitration_id = 0x5BF
        can.Message.data = [0x00, 0x00, 0x00, 0x50]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def reversing_on(bus):  # Reversing ON
        can.Message.arbitration_id = 0x390
        can.Message.dlc = 8
        can.Message.data = [0x20, 0x00, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        # can.Message.arbitration_id = 0x5BF
        # can.Message.data = [0x00, 0x00, 0x00, 0x50]
        # bus.send(polo_msg)  # Connect and send frame by CANable
        # time.sleep(0.1)

    @staticmethod
    def reversing_off(bus):  # Reversing OFF
        can.Message.arbitration_id = 0x390
        can.Message.dlc = 8
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        # can.Message.arbitration_id = 0x5BF
        # can.Message.data = [0x00, 0x00, 0x00, 0x50]
        # bus.send(polo_msg)  # Connect and send frame by CANable
        # time.sleep(0.1)

    @staticmethod
    def ILL_on(bus):  # Lighting ON
        can.Message.arbitration_id = 0x470
        can.Message.dlc = 3
        can.Message.data = [0x64, 0x00, 0x60]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def ILL_off(bus):  # Lighting OFF
        can.Message.arbitration_id = 0x470
        can.Message.dlc = 3
        can.Message.data = [0x00, 0x00, 0x00]
        bus.send(polo_msg)  # Connect and send frame by CANable
        time.sleep(0.1)