import time
import can

# -------------------------------------
#           variables
# -------------------------------------
nissan_msg = can.Message(arbitration_id=0x683,
                        data=[0, 00, 0, 0, 0, 0, 0, 0],
                        is_extended_id=False)


# ---------------------------------------------
# List of commands for Mitsubishi
# ---------------------------------------------
class Nissan:

    @staticmethod
    def can_bitrate():  # Mitsubishi CAN Bitrate
        return 500000

    @staticmethod
    def volume_up(bus):  # VoL+
        can.Message.arbitration_id = 0x683
        can.Message.dlc = 8
        can.Message.data = [0x04, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.01)
        can.Message.data = [0x04, 0x50, 0x20, 0x0D, 0x87, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.01)
        can.Message.data = [0x04, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.01)

    @staticmethod
    def volume_down(bus):  # VoL-
        can.Message.arbitration_id = 0x683
        can.Message.dlc = 8
        can.Message.data = [0x04, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.01)
        can.Message.data = [0x04, 0x00, 0x20, 0x0D, 0x86, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.01)
        can.Message.data = [0x04, 0x00, 0x00, 0x00, 0x06, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.01)

    @staticmethod
    def hang_up(bus):  # Hang Up
        can.Message.arbitration_id = 0x683
        can.Message.dlc = 8
        can.Message.data = [0x04, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x00, 0x20, 0x0D, 0x88, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def mode(bus):  # MODE
        can.Message.arbitration_id = 0x683
        can.Message.dlc = 8
        can.Message.data = [0x04, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x00, 0x20, 0x0D, 0x81, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def next_track(bus):  # NEXT TRACK
        can.Message.arbitration_id = 0x683
        can.Message.dlc = 8
        can.Message.data = [0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x00, 0x20, 0x0D, 0x82, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x00, 0x00, 0x00, 0x02, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def pre_track(bus):  # PREVIOUS TRACK
        can.Message.arbitration_id = 0x683
        can.Message.dlc = 8
        can.Message.data = [0x04, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x00, 0x20, 0x0D, 0x83, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def reversing_on(bus):  # Turn ON reversing
        can.Message.arbitration_id = 0x41F
        can.Message.dlc = 8
        can.Message.data = [0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def reversing_off(bus):  # Turn OFF reversing
        can.Message.arbitration_id = 0x41F
        can.Message.dlc = 8
        can.Message.data = [0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def acc_on(bus):  # ACC ON
        can.Message.arbitration_id = 0x6A8
        can.Message.dlc = 8
        can.Message.data = [0x04, 0x30, 0xC6, 0x2C, 0x04, 0xFF, 0xFF, 0xFF]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x07, 0x00, 0x00, 0x20, 0x04, 0x02, 0x02, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def acc_off(bus):  # ACC OFF
        can.Message.arbitration_id = 0x6A8
        can.Message.dlc = 8
        can.Message.data = [0x07, 0x00, 0x00, 0x20, 0x01, 0x02, 0x02, 0x00]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def voice_phone_short_press(bus):  # Short press voice/phone button
        can.Message.arbitration_id = 0x683
        can.Message.dlc = 8
        can.Message.data = [0x04, 0x30, 0x20, 0x0D, 0x84, 0xFF, 0xFF, 0xFF]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x40, 0x20, 0x0D, 0x84, 0xFF, 0xFF, 0xFF]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x50, 0x20, 0x0D, 0x04, 0xFF, 0xFF, 0xFF]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def voice_phone_long_press(bus):  # Long press voice/phone button
        can.Message.data = [0xF0, 0x0C, 0x0A, 0x01, 0xFF, 0xFF, 0xFF, 0xFF]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x10, 0x20, 0x0D, 0x84, 0xFF, 0xFF, 0xFF]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x20, 0x20, 0x0D, 0x84, 0xFF, 0xFF, 0xFF]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x30, 0x20, 0x0D, 0x84, 0xFF, 0xFF, 0xFF]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x40, 0x20, 0x0D, 0x84, 0xFF, 0xFF, 0xFF]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x50, 0x20, 0x0D, 0x84, 0xFF, 0xFF, 0xFF]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x04, 0x70, 0x20, 0x0D, 0x84, 0xFF, 0xFF, 0xFF]
        bus.send(nissan_msg)  # Connect and send frame by CANable
        time.sleep(0.1)