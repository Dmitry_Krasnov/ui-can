import time
import can

# -------------------------------------
#           variables
# -------------------------------------
mitsu_msg = can.Message(arbitration_id=0x315,
                        data=[0, 64, 0, 0, 0, 0, 0, 0],
                        is_extended_id=False)


# ---------------------------------------------
# List of commands for Mitsubishi
# ---------------------------------------------
class Mitsubishi:

    @staticmethod
    def can_bitrate():  # Mitsubishi CAN Bitrate
        return 250000

    @staticmethod
    def reversing_on(bus):  # Turn ON reversing
        can.Message.arbitration_id = 0x315
        can.Message.dlc = 8
        can.Message.data = [0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)
        time.sleep(0.1)

    @staticmethod
    def reversing_off(bus):  # Turn OFF reversing
        can.Message.arbitration_id = 0x315
        can.Message.dlc = 8
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)
        time.sleep(0.1)

    # Airconditiong display
    @staticmethod
    def foot_windscreen(bus):  # Foot+front mode
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        can.Message.data = [0x01, 0x2F, 0x2F, 0x06, 0x10, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x01, 0x2F, 0x2F, 0x06, 0x10, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x2F, 0x2F, 0x06, 0x10, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def face(bus):  # Face mode
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        can.Message.data = [0x01, 0x2F, 0x2F, 0x01, 0x10, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def face_foot(bus):  # Face+foot mode
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        can.Message.data = [0x01, 0x2F, 0x2F, 0x03, 0x10, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def foot(bus):  # Foot mode
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        can.Message.data = [0x01, 0x2F, 0x2F, 0x02, 0x10, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def dual(bus):  # DUAL mode
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        can.Message.data = [0x00, 0x3A, 0x3A, 0x02, 0x11, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def recirculation_cabin(bus):  # Cabin air recirculation mode
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        can.Message.data = [0x40, 0x1E, 0x39, 0x01, 0x21, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def recirculation_outside(bus):  # Outside air recirculation  mode
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        can.Message.data = [0x41, 0x1E, 0x39, 0x01, 0x11, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def ac_on(bus):  # AC ON
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        can.Message.data = [0x51, 0x1E, 0x39, 0x01, 0x18, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x50, 0x1E, 0x39, 0x01, 0x18, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def ac_off(bus):  # AC OFF
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        can.Message.data = [0x41, 0x1E, 0x39, 0x01, 0x18, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x40, 0x1E, 0x39, 0x01, 0x18, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def windscreen_heating_on(bus):  # Windscreen heating ON
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        can.Message.data = [0x40, 0x1E, 0x39, 0x04, 0x18, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def windscreen_heating_off(bus):  # Windscreen heating OFF
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        can.Message.data = [0x40, 0x1E, 0x39, 0x01, 0x18, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def rear_window_heating_on(bus):  # Rear window heating ON
        can.Message.arbitration_id = 0x424
        can.Message.dlc = 8
        can.Message.data = [0x98, 0x00, 0x0C, 0x01, 0x4D, 0xFE, 0x02, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x98, 0x00, 0x0C, 0x01, 0x4D, 0xFE, 0x02, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def rear_window_heating_off(bus):  # Rear window heating OFF
        can.Message.arbitration_id = 0x424
        can.Message.dlc = 8
        can.Message.data = [0x98, 0x00, 0x04, 0x01, 0x4D, 0xFE, 0x02, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x98, 0x00, 0x04, 0x01, 0x4D, 0xFE, 0x02, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def auto_on(bus):  # AUTO ON
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        can.Message.data = [0x31, 0x1E, 0x1E, 0x01, 0x18, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def auto_off(bus):  # AUTO OFF
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        can.Message.data = [0x11, 0x1E, 0x1E, 0x01, 0x18, 0x00, 0x00, 0x00]
        bus.send(mitsu_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def ventilator_power(bus):  # Ventilator power from 0 to max
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        min = 0x10
        max = 0x18
        cntr = min
        while cntr <= max:
            can.Message.data = [0x00, 0x1F, 0x1F, 0x01, cntr, 0x00, 0x00, 0x00]
            bus.send(mitsu_msg)  # Connect and send frame by CANable
            time.sleep(0.5)
            cntr = cntr + 1

    @staticmethod
    def left_tem_regulation(bus):  # left temperature regulation
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        min = 0x1E
        max = 0x3A
        cntr = min
        while cntr <= max:
            can.Message.data = [0x00, cntr, 0x02, 0x02, 0x11, 0x00, 0x00, 0x00]
            bus.send(mitsu_msg)  # Connect and send frame by CANable
            time.sleep(0.3)
            cntr = cntr + 1

    @staticmethod
    def right_tem_regulation(bus):  # right temperature regulation
        can.Message.arbitration_id = 0x186
        can.Message.dlc = 8
        min = 0x1E
        max = 0x3A
        cntr = min
        while cntr <= max:
            can.Message.data = [0x00, 0x02, cntr, 0x02, 0x11, 0x00, 0x00, 0x00]
            bus.send(mitsu_msg)  # Connect and send frame by CANable
            time.sleep(0.3)
            cntr = cntr + 1
