import time
import can

# -------------------------------------
#           variables
# -------------------------------------
vesta_msg = can.Message(arbitration_id=0x315,
                       data=[0, 64, 0, 0, 0, 0, 0, 0],
                       is_extended_id=False)


# ---------------------------------------------
# List of commands for Vesta
# ---------------------------------------------
class Vesta:

    @staticmethod
    def can_bitrate():  # Vesta CAN Bitrate
        return 500000

    @staticmethod
    def handbrake(bus):  # Handbrake (start AA)
        can.Message.arbitration_id = 0x4F8
        can.Message.dlc = 8
        can.Message.data = [0x58, 0x00, 0x10, 0x00, 0x00, 0x08, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x58, 0x00, 0x10, 0x00, 0x00, 0x08, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def Speed_5_km_h(bus):  # Speed about 5 km/h
        can.Message.arbitration_id = 0x5D7
        can.Message.dlc = 7
        can.Message.data = [0x00, 0x00, 0x00, 0x57, 0xA5, 0x70, 0xCC]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x57, 0xA5, 0x70, 0xCC]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def Speed_15_km_h(bus):  # Speed about 15 km/h
        can.Message.arbitration_id = 0x5D7
        can.Message.dlc = 7
        can.Message.data = [0x05, 0x18, 0x00, 0x57, 0xA5, 0x80, 0xD8]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x05, 0x18, 0x00, 0x57, 0xA5, 0x80, 0xD8]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def Reversing_on(bus):  # Reversing ON
        can.Message.arbitration_id = 0x55D
        can.Message.dlc = 8
        can.Message.data = [0x00, 0xED, 0x60, 0xC0, 0x92, 0x80, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0xED, 0x60, 0xC0, 0x92, 0x80, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def Reversing_off(bus):  # Reversing OFF
        can.Message.arbitration_id = 0x55D
        can.Message.dlc = 8
        can.Message.data = [0x00, 0xDD, 0x60, 0xC0, 0x92, 0x80, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0xDD, 0x60, 0xC0, 0x92, 0x80, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def wake_up(bus):
        can.Message.arbitration_id = 0x350
        can.Message.dlc = 8
        can.Message.data = [0xC7, 0x07, 0x18, 0x1F, 0x14, 0xC8, 0x94, 0x85]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0xC7, 0x07, 0x18, 0x1F, 0x14, 0xC8, 0x94, 0x85]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def Parktronic_zero_position(bus):
        can.Message.arbitration_id = 0x0C6
        can.Message.dlc = 8
        can.Message.data = [0x7F, 0xF1, 0x76, 0xBB, 0x80, 0x0F, 0xBE, 0x11]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x7F, 0xF1, 0x76, 0xBB, 0x80, 0x0F, 0xBE, 0x11]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def Parktronic_leftmost_position(bus):
        can.Message.arbitration_id = 0x0C6
        can.Message.dlc = 8
        can.Message.data = [0x94, 0x68, 0x81, 0x94, 0x80, 0x0F, 0xBA, 0xA5]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x94, 0x68, 0x81, 0x94, 0x80, 0x0F, 0xBA, 0xA5]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def Parktronic_rightmost_position(bus):
        can.Message.arbitration_id = 0x0C6
        can.Message.dlc = 8
        can.Message.data = [0x6B, 0x3B, 0x7F, 0x8D, 0x80, 0x0F, 0xB0, 0x0E]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x6B, 0x3B, 0x7F, 0x8D, 0x80, 0x0F, 0xB0, 0x0E]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def external_sensor_temperature_minus_18(bus):
        can.Message.arbitration_id = 0x3B7
        can.Message.dlc = 6
        can.Message.data = [0x16, 0x00, 0x7F, 0xC0, 0x00, 0xC0]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x16, 0x00, 0x7F, 0xC0, 0x00, 0xC0]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def external_sensor_temperature_0(bus):
        can.Message.arbitration_id = 0x3B7
        can.Message.dlc = 6
        can.Message.data = [0x28, 0x00, 0x7F, 0xC0, 0x00, 0xC0]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x28, 0x00, 0x7F, 0xC0, 0x00, 0xC0]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def external_sensor_temperature_plus_12(bus):
        can.Message.arbitration_id = 0x3B7
        can.Message.dlc = 6
        can.Message.data = [0x34, 0x00, 0x7F, 0xC0, 0x00, 0xC0]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x34, 0x00, 0x7F, 0xC0, 0x00, 0xC0]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def open_drivers_door(bus):
        can.Message.arbitration_id = 0x350
        can.Message.dlc = 8
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def open_passengers_door(bus):
        can.Message.arbitration_id = 0x350
        can.Message.dlc = 8
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def open_back_door(bus):
        can.Message.arbitration_id = 0x350
        can.Message.dlc = 8
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def open_hood(bus):
        can.Message.arbitration_id = 0x350
        can.Message.dlc = 8
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def open_trunk(bus):
        can.Message.arbitration_id = 0x350 # hex
        can.Message.dlc = 8
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x00, 0x04, 0x00, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    def Hood_Trunk_Doors(self, bus, vesta_open_trunk, vesta_open_hood, vesta_open_back_door, vesta_open_drivers_door, vesta_open_passengers_door):
        can.Message.arbitration_id = 0x350  # hex
        can.Message.dlc = 8
        can.Message.data = [0x00, 0x00, 0x00, 0x00]
        if vesta_open_hood == "on":
            can.Message.data = can.Message.data + [0x08]
        else:
            can.Message.data = can.Message.data + [0x04]
        can.Message.data = can.Message.data + [0x00]
        if vesta_open_drivers_door == "on":
            if vesta_open_passengers_door == "on":
                can.Message.data = can.Message.data + [0x28]
            else:
                can.Message.data = can.Message.data + [0x24]
        else:
            if vesta_open_passengers_door == "on":
                can.Message.data = can.Message.data + [0x18]
            else:
                can.Message.data = can.Message.data + [0x14]
        if vesta_open_back_door == "on":
            if vesta_open_trunk == "on":
                can.Message.data = can.Message.data + [0x0A]
            else:
                can.Message.data = can.Message.data + [0x02]
        else:
            if vesta_open_trunk == "on":
                can.Message.data = can.Message.data + [0x08]
            else:
                can.Message.data = can.Message.data + [0x05]
        print("Command:", can.Message.data)
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)






#Vesta.sendMultipleCommand(Vesta, 1, 'Open drivers door', 'Open passengers door')
