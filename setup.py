from setuptools import setup

APP = ['Main.py']
DATA_FILES = ['can_parser', 'car_methods', 'serial_port_connection', 'moon.icns', 'GUI.py', 'Thread.py', 'Var.py']
APP_NAME = "CANable_V2.0"
OPTIONS = {
    'argv_emulation': True,
    'iconfile': 'moon.icns'
}
setup(
    name=APP_NAME,
    app=APP,
    data_files=DATA_FILES,
    options={'py2app': OPTIONS},
    setup_requires=['py2app']
)