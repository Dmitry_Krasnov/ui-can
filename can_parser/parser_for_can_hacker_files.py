import time
import can

# ---------------------------------------------
msg = can.Message(arbitration_id=0x315,
                  data=[0, 0, 0, 0, 0, 0, 0, 0],
                  is_extended_id=True)

byte_list = [0, 0, 0, 0, 0, 0, 0, 0]


# ---------------------------------------------
# Read can files from CAN-HACKER and send data by CANable
# ---------------------------------------------

def parser(file, bus):
    with open(file) as fp:
        cntr = 0
        for text in fp:
            lines = text.strip().split('\r')
            for line in lines:
                cntr = cntr + 1
                if cntr == 1:  # skip the first line
                    continue
                elif cntr == 2:
                    continue  # skip the second line
                else:
                    # print("Line ({0}) {1}".format(cntr, line))
                    word = line.strip().split('\t')
                    cntr_symbol = 0
                    for symbol in word:
                        cntr_symbol = cntr_symbol + 1
                        # print("symbol ({0}) {1}".format(cntr_symbol, symbol))
                        if cntr_symbol == 4:
                            can.Message.arbitration_id = int(symbol, 16)
                        # print("ID: ({0}) {1}".format(cntr_symbol, symbol))
                        if cntr_symbol == 5:
                            can.Message.dlc = int(symbol)
                            amount_byte = int(symbol, 16)
                            # print("dlc: ({0}) {1}".format(cntr_symbol, symbol))
                            cntr_byte = 0
                            i = 0
                        if cntr_symbol == 6:
                            # print("Data: ({0}) {1}".format(cntr_symbol, symbol))
                            while cntr_byte != amount_byte:
                                byte = int(symbol[i:i + 2], 16)
                                # print(byte)
                                byte_list[cntr_byte] = byte
                                i = i + 3
                                # print("Frame.data_list:{0}".format(frame.data))
                                cntr_byte = cntr_byte + 1
                            can.Message.data = byte_list[0:amount_byte]
                            print("ID:{0}, Dlc:{1}, Data: {2}".format(can.Message.arbitration_id, can.Message.dlc,
                                                                      can.Message.data))
                            bus.send(msg)  # CAN frame out on the bus
                            time.sleep(0.001)
# ---------------------------------------------
# END
# ---------------------------------------------
